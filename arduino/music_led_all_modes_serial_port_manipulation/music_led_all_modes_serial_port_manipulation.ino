#include "FastLED.h"
//======================
//Command Related Items

#define SERIAL_PARAMETER_DELIMITER ','

String SERIAL_BUFFER;

int INT_COMMAND_PARAMETER;
String STRING_COMMAND_PARAMETER;
int COMMAND_PARAMETER_OFFSET;
String SERIAL_COMMAND;
char NEXT_BUFFER_CHAR;
bool COMMAND_PARAM_FINISHED;
bool COMMAND_PARAM_RESULT;

bool HasCommand;
int SERIAL_BUFF_INDEX;
int AnimationMode;
//animation modes
//1 - start to end
//2 - end to start
//3 - mirror
//4 - merge to middle
//5 - solid color
//6 - rock
#define ANIMATION_MODE_COUNT 6
//======================
#define NUM_LEDS 300        // How many leds in your strip?
#define updateLEDS 4        // How many do you want to update every millisecond?
#define COLOR_ORDER GRB
// Define color structure for rgb
struct color {
  int r;
  int g;
  int b;
};
typedef struct color Color;
Color nc;
CRGB leds[NUM_LEDS];        // Define the array of leds

// Define the digital I/O PINS..
#define DATA_PIN 12        // led data transfer




#define RED_PIN 8 //PWM Input channel 1
#define GREEN_PIN 9 //PWM Input channel 2
#define BLUE_PIN 11 //PWM Input channel 3
int RED_VALUE;
int GREEN_VALUE;
int BLUE_VALUE;

int section_sz=NUM_LEDS/2;
int si_1;
int si_2;
//==================================================
void setup() { 

    Serial.begin(115200);
    pinMode(RED_PIN,INPUT);
    pinMode(GREEN_PIN,INPUT);
    pinMode(BLUE_PIN,INPUT);
    pinMode(2,INPUT);
    DDRD = DDRD | B00000000;
    AnimationMode=2;
    FastLED.addLeds<WS2811, DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); //Set LED Type here -> WS2811, WS2812, WS2812B, WS2813
    FastLED.setDither( 0 );

    for(int i = 0; i < NUM_LEDS ; i++) {
      leds[i] = CRGB(0,0,0);
    }
    FastLED.show();
    delay(1000);
    for(int i = 0; i < NUM_LEDS ; i++) {
      leds[i] = CRGB(255,255,255);
    }
    FastLED.show();
    delay(5000);
    for(int i = 0; i < NUM_LEDS ; i++) {
      leds[i] = CRGB(0,0,0);
    }
    FastLED.show();
    Serial.println("INITIALIZED");
}
bool get_int_parameter(int offset)
{
  COMMAND_PARAM_FINISHED = false;
  COMMAND_PARAM_RESULT = false;
  STRING_COMMAND_PARAMETER="";
  while(!COMMAND_PARAM_FINISHED)
  {
    if(offset < SERIAL_BUFFER.length())
    {
      NEXT_BUFFER_CHAR = SERIAL_BUFFER[offset];
      if(NEXT_BUFFER_CHAR!=SERIAL_PARAMETER_DELIMITER)
      {
        STRING_COMMAND_PARAMETER+=NEXT_BUFFER_CHAR;
      }
      else
      {
        INT_COMMAND_PARAMETER = STRING_COMMAND_PARAMETER.toInt();
        COMMAND_PARAM_FINISHED = true;
        COMMAND_PARAM_RESULT = true;
      }
      offset++;
    }
    else
    {
      COMMAND_PARAM_FINISHED=true;
    }
  }
  COMMAND_PARAMETER_OFFSET=offset;
  return COMMAND_PARAM_RESULT;
}
bool get_string_parameter(int offset)
{
  COMMAND_PARAM_FINISHED = false;
  COMMAND_PARAM_RESULT = false;
  STRING_COMMAND_PARAMETER="";
  while(!COMMAND_PARAM_FINISHED)
  {
    if(offset < SERIAL_BUFFER.length())
    {
      NEXT_BUFFER_CHAR = SERIAL_BUFFER[offset];
      if(NEXT_BUFFER_CHAR!=SERIAL_PARAMETER_DELIMITER)
      {
        STRING_COMMAND_PARAMETER+=NEXT_BUFFER_CHAR;
      }
      else
      {
        COMMAND_PARAM_FINISHED = true;
        COMMAND_PARAM_RESULT = true;
      }
      offset++;
    }
    else
    {
      COMMAND_PARAM_FINISHED=true;
    }
  }
  COMMAND_PARAMETER_OFFSET=offset;
  return COMMAND_PARAM_RESULT;
}
void ProcessSerialCommand()
{
  SERIAL_BUFF_INDEX=0;
  if(get_string_parameter(0))
  {
    SERIAL_COMMAND = STRING_COMMAND_PARAMETER;
    SERIAL_BUFF_INDEX = COMMAND_PARAMETER_OFFSET;
    if(SERIAL_COMMAND=="SetAnimationMode")
    {
      
      if(get_int_parameter(SERIAL_BUFF_INDEX))
      {
        SERIAL_BUFF_INDEX += COMMAND_PARAMETER_OFFSET;
        int newAnimationMode = INT_COMMAND_PARAMETER;
        if(newAnimationMode > 0 && newAnimationMode <= ANIMATION_MODE_COUNT)
        {
          Serial.print("New Animation Mode: ");
          Serial.println(newAnimationMode);
          AnimationMode = newAnimationMode;
        }
      }
    }
  }
}
void loop() { 

  if(bitRead(PIND,PD2))
  {
    
    while(Serial.available() > 0)
    {
      char c = Serial.read();
      
      if(c == '\n')
      {
        ProcessSerialCommand();
        SERIAL_BUFFER = "";
      }
      else
      {
        SERIAL_BUFFER += c;
      }
    }
  }
  else
  {
    
    setColor(&nc,0,0,0);
  
    RED_VALUE=pulseIn(RED_PIN,HIGH); 
    GREEN_VALUE=pulseIn(GREEN_PIN,HIGH); 
    BLUE_VALUE=pulseIn(BLUE_PIN,HIGH); 
  
    setColor(&nc,map(RED_VALUE,4,1265,0,255),map(GREEN_VALUE,4,1265,0,255),map(BLUE_VALUE,4,1265,0,255));//map from 1 to 1025 to 0 to 255

    if(AnimationMode==1)
    {
      StartToEnd();
    }
    else if(AnimationMode==2)
    {
      EndToStart();
    }
    else if(AnimationMode==3)
    {
      Mirror();
    }
    else if(AnimationMode==4)
    {
      MergeToMiddle();
    }
    else if(AnimationMode==5)
    {
      SolidColor();
    }
    else if(AnimationMode==6)
    {
      RockMode();
    }
    
    FastLED.show();
    //FastLED.delay(1000/240);  //you may or may not need this. Replace FramesPerSecond with desired FPS. Standard shows run between 20-44, PC monitors run at 60-144.
    //delay(1);
    //print_signals();
    //printColor(nc);
    //delay(3);
  }
}

void setColor(Color *c, int r, int g, int b) {
  c->r = r;
  c->g = g;
  c->b = b;
}


//=======================================
//Animation Modes
//1 - start to end
//2 - end to start
//3 - mirror
//4 - merge to middle
//5 - solid color
//6 - Rock
void StartToEnd()
{
  // Shift all LEDs to the right by updateLEDS number each time
  for(int i = NUM_LEDS - 1; i >= updateLEDS; i--) {
    leds[i] = leds[i - updateLEDS];
  }
   // Set the left most updateLEDs with the new color
  for(int i = 0; i < updateLEDS; i++) {
    leds[i] = CRGB(nc.r, nc.g, nc.b);
  }
}
void EndToStart()
{
  for(int i = 0; i < NUM_LEDS-updateLEDS; i++)
  {
    leds[i] = leds[i+updateLEDS];
  }

  for(int i = 0; i < updateLEDS; i++)
  {
    leds[NUM_LEDS-1-i] = CRGB(nc.r, nc.g, nc.b);
  }
}
void Mirror()
{
   for(int i = NUM_LEDS - 1; i >= updateLEDS+section_sz; i--) {
    leds[i] = leds[i - updateLEDS];
    leds[NUM_LEDS-1-i] = leds[NUM_LEDS-1-i+updateLEDS];
    
  }
   for(int i = section_sz; i < section_sz+updateLEDS; i++) {
    leds[i] = CRGB(nc.r, nc.g, nc.b);
    leds[i-updateLEDS] = CRGB(nc.r, nc.g, nc.b);
  }

}
void MergeToMiddle()
{
  for(int i = section_sz-1; i >= updateLEDS; i--) {
    leds[i] = leds[i - updateLEDS];
    leds[NUM_LEDS-1-i] = leds[NUM_LEDS-1-i+updateLEDS];

  }
  for(int i = 0; i < updateLEDS; i++) {
    leds[i] = CRGB(nc.r, nc.g, nc.b);
    leds[NUM_LEDS-1-i] = CRGB(nc.r, nc.g, nc.b);
  }
}
void SolidColor()
{
  for(int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(nc.r, nc.g, nc.b);
  }
}
void RockMode()
{
  
}
//=======================================


void print_signals(){
  
  Serial.print(RED_VALUE);
    Serial.print(" | ");
  Serial.print(GREEN_VALUE);
    Serial.print(" | ");
  Serial.println(BLUE_VALUE);
  
}