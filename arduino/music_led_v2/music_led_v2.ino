#include "FastLED.h"

#define NUM_LEDS 300        // How many leds in your strip?
#define updateLEDS 8        // How many do you want to update every millisecond?
#define COLOR_ORDER GRB
#define FASTLED_ALLOW_INTERRUPTS 1

CRGB leds[NUM_LEDS];        // Define the array of leds

// Define the digital I/O PINS..
#define DATA_PIN 12        // led data transfer
#define RED_PIN 8 //PWM Input channel 1
#define GREEN_PIN 9 //PWM Input channel 2
#define BLUE_PIN 10 //PWM Input channel 3
byte last_state_red, last_state_blue, last_state_green;
volatile int red_value,green_value,blue_value;
unsigned long red_timer,green_timer,blue_timer;
volatile bool red_ready,green_ready,blue_ready;

// Define color structure for rgb
struct color {
  int r;
  int g;
  int b;
};
typedef struct color Color;

//==================================================
ISR(PCINT0_vect)
{
  if(!red_ready && last_state_red == 0 && PINB & B00000001)
  {
    last_state_red = 1;
    red_timer=micros();
  }
  else if(!red_ready && last_state_red == 1 && !(PINB & B00000001))
  {
    last_state_red=0;
    red_value=micros()-red_timer;
    red_ready=true;
  }
  
  if(!green_ready && last_state_green == 0 && PINB & B00000010)
  {
    last_state_green = 1;
    green_timer=micros();
  }
  else if(!green_ready && last_state_green == 1 && !(PINB & B00000010))
  {
    last_state_green=0;
    green_value=micros()-green_timer;
    green_ready=true;
  }


  if(!blue_ready && last_state_blue == 0 && PINB & B00000100)
  {
    last_state_blue = 1;
    blue_timer=micros();
  }
  else if(!blue_ready && last_state_blue == 1 && !(PINB & B00000100))
  {
    last_state_blue=0;
    blue_value=micros()-blue_timer;
    blue_ready=true;
  }
}
//==================================================
void setup() { 

    //Serial.begin(115200);
    pinMode(RED_PIN,INPUT);
    pinMode(GREEN_PIN,INPUT);
    pinMode(BLUE_PIN,INPUT);

    //==================================================

    PCICR |= (1 <<PCIE0);
    PCMSK0 |= (1 <<PCINT0);
    PCMSK0 |= (1 <<PCINT1);
    PCMSK0 |= (1 <<PCINT2);
    
    //==================================================
    
    FastLED.addLeds<WS2811, DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); //Set LED Type here -> WS2811, WS2812, WS2812B, WS2813
    FastLED.setDither( 0 );

    for(int i = 0; i < NUM_LEDS ; i++) {
      leds[i] = CRGB(0,0,0);
    }
    FastLED.show();
    delay(1000);
    for(int i = 0; i < NUM_LEDS ; i++) {
      leds[i] = CRGB(255,255,255);
    }
    FastLED.show();
    delay(5000);
    for(int i = 0; i < NUM_LEDS ; i++) {
      leds[i] = CRGB(0,0,0);
    }
    FastLED.show();
    
}

void loop() { 

  // Shift all LEDs to the right by updateLEDS number each time
  for(int i = NUM_LEDS - 1; i >= updateLEDS; i--) {
    leds[i] = leds[i - updateLEDS];
  }


  Color nc;
  setColor(&nc,0,0,0);

//read pins+set new color
  //RED_VALUE=pulseIn(RED_PIN,HIGH)/8; //this is an 8-bit pwm signal so we can divide by 8 to reduce "noise"
  //GREEN_VALUE=pulseIn(GREEN_PIN,HIGH)/8; //this is an 8-bit pwm signal so we can divide by 8 to reduce "noise"
  //BLUE_VALUE=pulseIn(BLUE_PIN,HIGH)/8; //this is an 8-bit pwm signal so we can divide by 8 to reduce "noise"
  //while(!red_ready || !green_ready || !blue_ready)
  //{
  //
  //}
  if(red_ready && green_ready && blue_ready)
  {
    //print_signals();
    setColor(&nc,map(red_value/8,2,952,0,255),map(green_value/8,2,952,0,255),map(blue_value/8,2,952,0,255));//map from 1 to 1025 to 0 to 255
    // Set the left most updateLEDs with the new color
    red_ready=false;
    green_ready=false;
    blue_ready=false;
    for(int i = 0; i < updateLEDS; i++) {
      leds[i] = CRGB(nc.r, nc.g, nc.b);
    }
  }
  else
  {
    for(int i = 0; i < updateLEDS; i++) {
      leds[i] = CRGB(nc.r, nc.g, nc.b);
    }
  }
  FastLED.show();
  FastLED.delay(1000/240);  //you may or may not need this. Replace FramesPerSecond with desired FPS. Standard shows run between 20-44, PC monitors run at 60-144.
  //delay(1);
  //print_signals();
  //printColor(nc);
  //delay(1000);
}
void print_signals(){
  
  Serial.print(red_value/8);
    Serial.print(" | ");
  Serial.print(green_value/8);
    Serial.print(" | ");
  Serial.println(blue_value/8);
  
}



void setColor(Color *c, int r, int g, int b) {
  c->r = r;
  c->g = g;
  c->b = b;
}

// Prints color structure data
void printColor(Color c) {
  Serial.print("( ");
  Serial.print(c.r);
  Serial.print(", ");
  Serial.print(c.g);
  Serial.print(", ");
  Serial.print(c.b);
  Serial.println(" )");
}