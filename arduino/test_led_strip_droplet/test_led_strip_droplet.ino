#include <FastLED.h>
#define NUM_LEDS 300

#define LED_TYPE WS2812B
#define COLOR_ORDER GRB
#define DATA_PIN 12
#define BRIGHTNESS  255

CRGB leds[NUM_LEDS];


void setup() { 
  Serial.begin(9600);
  FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS)    .setCorrection(TypicalLEDStrip)
    .setDither(BRIGHTNESS < 255);
}

void loop() {
  for(int dot = 0; dot < NUM_LEDS; dot++) { 
    leds[dot] = CRGB(0,0,255);
    FastLED.show();
    leds[dot] = CRGB(0,0,0);
    delay(1000);
  }
}
