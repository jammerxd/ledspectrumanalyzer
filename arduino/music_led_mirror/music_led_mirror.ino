#include "FastLED.h"

#define NUM_LEDS 300        // How many leds in your strip?
#define updateLEDS 4       // How many do you want to update every millisecond?
#define COLOR_ORDER GRB

CRGB leds[NUM_LEDS];        // Define the array of leds

// Define the digital I/O PINS..
#define DATA_PIN 12        // led data transfer


// Define color structure for rgb
struct color {
  int r;
  int g;
  int b;
};
typedef struct color Color;

#define RED_PIN 8 //PWM Input channel 1
#define GREEN_PIN 9 //PWM Input channel 2
#define BLUE_PIN 11 //PWM Input channel 3
int RED_VALUE;
int GREEN_VALUE;
int BLUE_VALUE;

int section_sz=NUM_LEDS/2;
int si_1;
int si_2;
//==================================================
void setup() { 

    //Serial.begin(115200);
    pinMode(RED_PIN,INPUT);
    pinMode(GREEN_PIN,INPUT);
    pinMode(BLUE_PIN,INPUT);
    FastLED.addLeds<WS2811, DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS); //Set LED Type here -> WS2811, WS2812, WS2812B, WS2813
    FastLED.setDither( 0 );

    for(int i = 0; i < NUM_LEDS ; i++) {
      leds[i] = CRGB(0,0,0);
    }
    FastLED.show();
    delay(1000);
    for(int i = 0; i < NUM_LEDS ; i++) {
      leds[i] = CRGB(255,255,255);
    }
    FastLED.show();
    delay(5000);
    for(int i = 0; i < NUM_LEDS ; i++) {
      leds[i] = CRGB(0,0,0);
    }
    FastLED.show();

}

void loop() { 

  Color nc;
  setColor(&nc,0,0,0);

  RED_VALUE=pulseIn(RED_PIN,HIGH); 
  GREEN_VALUE=pulseIn(GREEN_PIN,HIGH); 
  BLUE_VALUE=pulseIn(BLUE_PIN,HIGH); 
  setColor(&nc,map(RED_VALUE,4,1265,0,255),map(GREEN_VALUE,4,1265,0,255),map(BLUE_VALUE,4,1265,0,255));//map from 1 to 1025 to 0 to 255
  
  for(int i = NUM_LEDS - 1; i >= updateLEDS+section_sz; i--) {
    leds[i] = leds[i - updateLEDS];
    leds[NUM_LEDS-1-i] = leds[NUM_LEDS-1-i+updateLEDS];
    
  }
   for(int i = section_sz; i < section_sz+updateLEDS; i++) {
    leds[i] = CRGB(nc.r, nc.g, nc.b);
    leds[i-updateLEDS] = CRGB(nc.r, nc.g, nc.b);
  }

  
  
  FastLED.show();
  FastLED.delay(1000/240);  //you may or may not need this. Replace FramesPerSecond with desired FPS. Standard shows run between 20-44, PC monitors run at 60-144.
  //delay(1);
  //print_signals();
  //printColor(nc);
  //delay(3);
}
void print_signals(){
  
  Serial.print(RED_VALUE);
    Serial.print(" | ");
  Serial.print(GREEN_VALUE);
    Serial.print(" | ");
  Serial.println(BLUE_VALUE);
  
}



void setColor(Color *c, int r, int g, int b) {
  c->r = r;
  c->g = g;
  c->b = b;
}

// Prints color structure data
void printColor(Color c) {
  Serial.print("( ");
  Serial.print(c.r);
  Serial.print(", ");
  Serial.print(c.g);
  Serial.print(", ");
  Serial.print(c.b);
  Serial.println(" )");
}