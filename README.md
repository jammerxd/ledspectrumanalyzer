# LEDSpectrumAnalyzer

Software for LED Spectrum Analysis(i.e. Syncing LEDs over network to music playing on a PC)

This repository consists of a few main parts.

LEDControllerClient and LEDControllerClient.sln -> this holds the client software that generates the audio spectrum and configures the controller(s) to connect to.

Arduino -> the arduino sketch begin used to send data to the addressable LED strip

LEDControllerServer -> The server written in python running on a raspberry pi. 

Chart.html => an html file that shows the curve of my default profiles in relation to reported volume/loudness of that frequency "bin"



# LEDControllerClient

I rewrote the example from  https://www.codeproject.com/Articles/797537/Making-an-Audio-Spectrum-analyzer-with-Bass-dll-Cs and used additional resea rch to complete this project.

An honorable mention goes to https://www.csharpcodi.com/vs2/1205/CUE.NET/Examples/AudioAnalyzer/Example_AudioAnalyzer_full/TakeAsIs/SoundDataProcessor.cs/

The analyzer class has been completely rewritten to be more friendly to MVVM and WPF. Can be configured using the specifid device index and change the number of bars shown for the spectrum.

You can configure a profile for a specific "bin" of audio data to adjust the curve of the louds and softs. There is no limit to how many profiles you can make o rhow many items you can have in the list, but note that the analyzer reports a number from 0-100 so 101 would be the theoretical maximum for 1 to 1 mappings.



# Arduino

The arduino sketch uses FastLED and will need to be configured using your own strip. You'll need to research this on your own.

The sketch is based on a project done by a youtuber and uses modified code:

Video: https://www.youtube.com/watch?v=lU1GVVU9gLU

Source: https://github.com/DevonCrawford/LED-Music-Visualizer

There's music_led which is the sketch I'm using for the music visualization, but I included samples from other users as well.

SimpleNeoPixelDemo is for demonstrating how to control a strip using no external libraries while being fast and efficient. Can be fo und at: https://github.com/bigjosh/SimpleNeoPixelDemo

The test_led_strip_droplet is for testing the strip by sending a single droplet down the strip.





# LEDControllerServer

This is the server that runs on the Raspberry Pi. Built using python3 and uses a PCA9865 expansion board to send out PWM signals as the outputs.

The arduino can read these PWM signals as inputs.




# BassWasapi

I used Bass.NET and supporting libraries to make the analyzer. This article hel;ped me understand the match required for FFT's: http://www.un4seen.com/forum/?topic=4941.0




# Disclaimer

This software is provided free of charge with no  guarantee of updates, fixes, or maintenance of any kind. I am not responsible for any damage(s) that may occur as a result of using this software.;