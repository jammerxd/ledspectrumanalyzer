﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using LEDControllerClient.Class;
using Lifx;
namespace LEDControllerClient.Model.LIFX
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class LIFXA19BulbController : Interface.LEDController, IDisposable
    {
        static LightFactory factory = new LightFactory();
        public LIFXA19BulbController(string name, string IPAddress,int spectrumBand, float multiplier,System.Drawing.Color color) : base("LIFX A19 Bulb",name)
        {
            this.IPAddress = System.Net.IPAddress.Parse(IPAddress);
            needsReset = true;
            run = false;
            isReadyForNext = false;
            musicChannel.SelectedMusicPorfile = MusicProfileConfig.Profiles.Where(x => x.Name == "Peaks-Only-Aggressive").First();
            musicChannel.AudioSpectrumBandIndex = spectrumBand;
            musicChannel.Multiplier = multiplier;
            this.color = color;
            Messenger.Default.Register<AnalyzerDataEventArgs>(this, AnalyzerData);
            
        }

        private void AnalyzerData(AnalyzerDataEventArgs obj)
        {
            //redMusicValue = Convert.ToInt32((double)this.RedChannel.SelectedMusicPorfile.Values[obj.Data.Spectrum[this.RedChannel.AudioSpectrumBandIndex]] * (double)2.55 * (double)this.RedChannel.Multiplier);
            //greenMusicValue = Convert.ToInt32((double)this.GreenChannel.SelectedMusicPorfile.Values[obj.Data.Spectrum[this.GreenChannel.AudioSpectrumBandIndex]] * (double)2.55 * (double)this.GreenChannel.Multiplier);
            //blueMusicValue = Convert.ToInt32((double)this.BlueChannel.SelectedMusicPorfile.Values[obj.Data.Spectrum[this.BlueChannel.AudioSpectrumBandIndex]] * (double)2.55 * (double)this.BlueChannel.Multiplier);
            if (needsReset && run)
            {
                Task.Run(() => Reset()).ConfigureAwait(false);
            }
            else if(run)
            {
                Task.Run(() => { updateThread_Elapsed(obj.Data,musicChannel); }).ConfigureAwait(false);
            }
        }

        private void updateThread_Elapsed(SpectrumData data,LEDMusicChannel channel)
        {
            if (!needsReset && isReadyForNext && run)
            {
                isReadyForNext = false;
                try
                {
                    if (data.Spectrum[1] == 100 && data.Spectrum[2] == 100)
                    { 
                        Task.WaitAll(
                            light.SetColorAsync(Color.White),
                            light.SetBrightnessAsync(1.0, 0)
                        );
                    }
                    else
                    {
                        double brightness = channel.SelectedMusicPorfile.Values[data.Spectrum[channel.AudioSpectrumBandIndex]] * (double)0.001 * (double)channel.Multiplier;
                        light.SetBrightnessAsync(brightness, 20).RunSynchronously();
                    }
                    isReadyForNext = true;
                }
                catch
                {
                    needsReset = true;
                }
            }
        }

        public void Reset()
        {
            Disconnect();
            Connect();
        }
        public void Connect()
        {
            try
            {
                if (factory != null)
                {
                    light = factory.CreateLightAsync(this.IPAddress).Result;
                    light.OnAsync().RunSynchronously();
                    
                    float hue = color.GetHue();
                    float saturation = color.GetSaturation();
                    float lightness = color.GetBrightness();
                    Task.WaitAll(
                        light.SetColorAsync(new Color(new Hue(Convert.ToInt32(Math.Round(color.GetHue(), 0))), new Percentage(Convert.ToDouble(color.GetSaturation())))),
                        light.SetBrightnessAsync(color.GetBrightness())
                    );
                    
                }
                needsReset = false;
                run = true;
                isReadyForNext = true;
            }
            catch
            {
                needsReset = true;
                run = false;
                isReadyForNext = false;
            }
        }
        public void Disconnect()
        {
            try
            {
                if (light != null)
                {
                    using (light) { }
                }
            }
            catch
            {

            }
            finally
            {
                needsReset = true;
                isReadyForNext = false;
                run = false;
            }
        }
        public void Dispose()
        {
            run = false;
            Disconnect();
        }
        private ILight light;
        private volatile bool run;
        private volatile bool needsReset;
        private volatile bool isReadyForNext;
        private System.Net.IPAddress IPAddress;

        private LEDMusicChannel musicChannel = new LEDMusicChannel();
        private MusicProfileConfig MusicProfileConfig = Model.MusicProfileConfig.LoadFromConfig(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "SpectrumProfiles.json"));
        private System.Drawing.Color color;

       
    }
}
