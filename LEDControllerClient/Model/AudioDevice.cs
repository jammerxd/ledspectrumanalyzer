﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDControllerClient.Model
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class AudioDevice
    {
        public int DeviceID { get; set; }
        public string DisplayName { get; set; }

    }
}
