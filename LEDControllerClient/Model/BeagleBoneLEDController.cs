﻿using GalaSoft.MvvmLight.Messaging;
using LEDControllerClient.Class;
using LEDControllerClient.Message;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LEDControllerClient.Model
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class BeagleBoneLEDController : Interface.LEDController,IDisposable
    {
        public BeagleBoneLEDController(string Name,string IPAddress, int Port, bool UseUDP, int Count, int OPCChannel) :base("Beagle Bone Black",Name)
        {
            this.Pixels = new ObservableCollection<System.Drawing.Color>();
            /*this.pixelTimer = new Timer();
            this.pixelTimer.Interval = 1;
            this.pixelTimer.Enabled = true;
            this.pixelTimer.AutoReset = true;
            this.pixelTimer.Elapsed += PixelTimer_Elapsed;
            this.pixelTimer.Start();*/
            this.IPAddress = IPAddress;
            this.Port = Port;
            this.UseUDP = UseUDP;
            this.PixelCount = Count;
            this.run = true;
            for(int i = 0; i < this.PixelCount; i++)
            {
                this.Pixels.Add(System.Drawing.Color.FromArgb(0, 0, 0));
            }
            this.client = new AsyncTCP.Client.AsynchronousClient();

            this.RedChannel = new LEDMusicChannel();
            this.GreenChannel = new LEDMusicChannel();
            this.BlueChannel = new LEDMusicChannel();

            this.RedChannel.AudioSpectrumBandIndex = 2;
            this.GreenChannel.AudioSpectrumBandIndex = 1;
            this.BlueChannel.AudioSpectrumBandIndex = 4;
            this.RedChannel.SelectedMusicPorfile = MusicProfileConfig.Profiles.Where(x => x.Name == "Peaks-Only-Aggressive").First();
            this.GreenChannel.SelectedMusicPorfile = MusicProfileConfig.Profiles.Where(x => x.Name == "Peaks-Only-Aggressive").First();
            this.BlueChannel.SelectedMusicPorfile = MusicProfileConfig.Profiles.Where(x => x.Name == "Peaks-Only-Aggressive").First();
            /*this.RedChannel.SelectedMusicPorfile = MusicProfileConfig.Profiles.Where(x => x.Name == "Normal").First();
            this.GreenChannel.SelectedMusicPorfile = MusicProfileConfig.Profiles.Where(x => x.Name == "Normal").First();
            this.BlueChannel.SelectedMusicPorfile = MusicProfileConfig.Profiles.Where(x => x.Name == "Normal").First();*/
            this.RedChannel.Multiplier = 0.8f;
            this.GreenChannel.Multiplier = 1.0f;
            this.BlueChannel.Multiplier = 0.8f;
            this.OPCChannel = OPCChannel;
            this.AnimationTypes = new ObservableCollection<string>() { "Music" };
            this.SelectedAnimationType = this.AnimationTypes.First();

            this.section_sz = Count / 2;
            this.updateLEDs = 3;
            this.pixelThread = new System.Threading.Thread(this.PixelTimer_Elapsed);
            this.pixelThread.IsBackground = true;
            if (this.pixelThread.ThreadState != System.Threading.ThreadState.Running)
            {
                this.pixelThread.Start();
            }
            Messenger.Default.Register<AnalyzerDataEventArgs>(this, AnalyzerData);
            

        }

        private void PixelTimer_Elapsed()
        {
            while (run)
            {
                if (this.client.IsConnected() && SelectedAnimationType == "Music")
                {
                    System.Drawing.Color _nextColor = System.Drawing.Color.FromArgb(redMusicValue, greenMusicValue, blueMusicValue);
                    /*if(lastRedMusicValue == redMusicValue && lastGreenMusicValue == greenMusicValue && lastBlueMusicValue == blueMusicValue)
                    {
                        _nextColor = System.Drawing.Color.FromArgb(0, 0, 0);
                    }*/
                    for (int i = this.PixelCount - 1; i >= this.updateLEDs + section_sz; i--)
                    {
                        this.Pixels[i] = this.Pixels[i - this.updateLEDs];
                        this.Pixels[this.PixelCount - 1 - i] = this.Pixels[this.PixelCount - 1 - i + this.updateLEDs];

                    }
                    for (int i = section_sz; i < section_sz + this.updateLEDs; i++)
                    {
                        this.Pixels[i] = _nextColor;
                        this.Pixels[i - this.updateLEDs] = _nextColor;
                    }

                    this.client.SendBytes(BuildPixels().ToArray(), false);

                    lastBlueMusicValue = _nextColor.B;
                    lastGreenMusicValue = _nextColor.G;
                    lastRedMusicValue = _nextColor.R;

                }
                System.Threading.Thread.Sleep(12);
            }
        }

        private void AnalyzerData(AnalyzerDataEventArgs obj)
        {
            redMusicValue = Convert.ToInt32((double)this.RedChannel.SelectedMusicPorfile.Values[obj.Data.Spectrum[this.RedChannel.AudioSpectrumBandIndex]] * (double)2.55 * (double)this.RedChannel.Multiplier);
            greenMusicValue = Convert.ToInt32((double)this.GreenChannel.SelectedMusicPorfile.Values[obj.Data.Spectrum[this.GreenChannel.AudioSpectrumBandIndex]] * (double)2.55 * (double)this.GreenChannel.Multiplier);
            blueMusicValue = Convert.ToInt32((double)this.BlueChannel.SelectedMusicPorfile.Values[obj.Data.Spectrum[this.BlueChannel.AudioSpectrumBandIndex]] * (double)2.55 * (double)this.BlueChannel.Multiplier);
        }

        private IEnumerable<byte> BuildPixels()
        {
            List<byte> bytesToSend = new List<byte>();
            var lenHiByte = (this.PixelCount * 3) / 256;
            var lenLoByte = (this.PixelCount * 3) % 256;
            byte[] header = { (byte)this.OPCChannel, (byte)0, (byte)lenHiByte, (byte)lenLoByte };
            bytesToSend.AddRange(header);
            foreach (var pixel in this.Pixels)
            {
                bytesToSend.AddRange(new List<byte>() { (byte)pixel.R, (byte)pixel.G, (byte)pixel.B });
            }
            return bytesToSend;

        }

        public string IPAddress { get; set; }
        public int Port { get; set; }
        public bool UseUDP { get; set; }
        public int PixelCount { get; set; }

        public LEDMusicChannel RedChannel { get; set; }
        public LEDMusicChannel GreenChannel { get; set; }
        public LEDMusicChannel BlueChannel { get; set; }
        public int OPCChannel { get; set; }
        public ObservableCollection<string> AnimationTypes { get; set; }
        public String SelectedAnimationType { get; set; }



        public ObservableCollection<System.Drawing.Color> Pixels { get; set; }
        private AsyncTCP.Client.AsynchronousClient client;

        private int updateLEDs = 3;
        private int section_sz;
        private MusicProfileConfig MusicProfileConfig = Model.MusicProfileConfig.LoadFromConfig(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "SpectrumProfiles.json"));
        //private Timer pixelTimer;
        private volatile int redMusicValue;
        private volatile int greenMusicValue;
        private volatile int blueMusicValue;

        private int lastRedMusicValue;
        private int lastGreenMusicValue;
        private int lastBlueMusicValue;
        private volatile bool run;

        private System.Threading.Thread pixelThread;

        public void Connect()
        {
            this.client.Connect(this.IPAddress, this.Port, this.UseUDP ? System.Net.Sockets.ProtocolType.Udp : System.Net.Sockets.ProtocolType.Tcp);
        }
        public void Disconnect()
        {
            this.client.Disconnect();
        }

        public void Dispose()
        {
            this.run = false;
            System.Threading.Thread.Sleep(50);
            Disconnect();
        }
    }
}
