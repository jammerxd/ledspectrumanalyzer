﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDControllerClient.Model
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class MusicProfileConfig
    {
        MusicProfileConfig()
        {
            Profiles = new ObservableCollection<MusicProfile>();
        }
        public ObservableCollection<MusicProfile> Profiles { get; set; }

        public static MusicProfileConfig LoadFromConfig(string file)
        {
            if(System.IO.File.Exists(file))
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<MusicProfileConfig>(System.IO.File.ReadAllText(file));
            }
            else
            {
                System.IO.File.WriteAllText(file, Newtonsoft.Json.JsonConvert.SerializeObject(new MusicProfileConfig()));
            }
            return new MusicProfileConfig();
        }
    }
    public class MusicProfile
    { 
        public string Name { get; set; }
        public ObservableCollection<int> Values { get; set; }
        public MusicProfile()
        {
            Name = "NO NAME";
            Values = new ObservableCollection<int>();
        }
        public int GetAdjustedValue(int val, float preMultiplier=1.0f, float postMultiplier=1.0f)
        {
            int pre = (int)Math.Round(val * preMultiplier);
            if(Values.Count <= pre)
            {
                pre = Values.Count - 1;
            }
            int post = (int)Math.Round(Values[pre] * postMultiplier);
            return post;
        }
    }
}
