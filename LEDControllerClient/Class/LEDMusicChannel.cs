﻿using LEDControllerClient.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDControllerClient.Class
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class LEDMusicChannel
    {

        public MusicProfile SelectedMusicPorfile { get; set; }
        public int AudioSpectrumBandIndex { get; set; }
        public float Multiplier { get; set; }
       
    }

}
