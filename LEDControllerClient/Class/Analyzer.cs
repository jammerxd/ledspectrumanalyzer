﻿using GalaSoft.MvvmLight.Messaging;
using LEDControllerClient.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Un4seen.Bass;
using Un4seen.BassWasapi;

namespace LEDControllerClient.Class
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class AnalyzerOperationResult
    {
        public bool Error { get; set; }
        public Exception Exception { get; set; }
    }
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class AnalyzerDataEventArgs : EventArgs
    {
        public SpectrumData Data {get;set;}
    }


    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class Analyzer : IDisposable
    {
        public event EventHandler AnalyzerData;
        public delegate void AnalyzerDataEventHandler(object sender, AnalyzerDataEventArgs e);
        public virtual void OnAnalyzerData(object sender, AnalyzerDataEventArgs e)
        {
            EventHandler handler = AnalyzerData;
            handler?.Invoke(this, e);
        }

        public bool Enabled { get; private set; }
        public bool Initialized { get; private set; }
        public int SpectrumCount { get; private set; }
        public int FrameDelay { get; private set; }

        public AudioDevice AudioDevice { get; private set; }

        private DispatcherTimer dispatchTimer { get; set; }
        private float[] FFT { get; set; }
        private WASAPIPROC WASAPIPROC { get; set; }
        private bool IsBASSInitialized { get; set; }




        public Analyzer()
        {
            FFT = new float[1024];
            WASAPIPROC = new WASAPIPROC(WASAPI_Process);
            Enabled = false;
            Initialized = false;
            FrameDelay = 8;

            dispatchTimer = new DispatcherTimer();
            dispatchTimer.IsEnabled = false;
            dispatchTimer.Interval = TimeSpan.FromMilliseconds(FrameDelay);
            dispatchTimer.Tick += DispatchTimer_Tick;

            IsBASSInitialized = false;
            SetSpectrumCount(7);
            SetFrameDelay(8);
            Messenger.Default.Send(new AnalyzerDataEventArgs() { Data = new SpectrumData() { LeftLevel = 0, RightLevel = 0, Spectrum = new List<int>() { 0, 0, 0, 0, 0, 0, 0 } } }) ;
            OnAnalyzerData(this, new AnalyzerDataEventArgs() { Data = new SpectrumData() { LeftLevel = 0, RightLevel = 0, Spectrum = new List<int>() { 0, 0, 0, 0, 0, 0, 0 } } });



        }

        public AnalyzerOperationResult SetSpectrumCount(int newCount)
        {
            //TODO: Stop and Re-Sync?
            bool wasEnabled = Enabled;
            if (wasEnabled)
            {
                var disableResult = Disable();
                if (disableResult.Error)
                {
                    return disableResult;
                }
            }

            SpectrumCount = newCount;
            if (wasEnabled)
            {
                return Enable();
            }
            return new AnalyzerOperationResult() { Error = false };
            //TODO: Resume?


        }
        public AnalyzerOperationResult SetFrameDelay(int newFrameDelay)
        {
            //TODO: Stop and Re-Sync?
            bool wasEnabled = Enabled;
            if (wasEnabled)
            {
                var disableResult = Disable();
                if (disableResult.Error)
                {
                    return disableResult;
                }
            }
            FrameDelay = newFrameDelay;
            dispatchTimer.Interval = TimeSpan.FromMilliseconds(FrameDelay);
            if (wasEnabled)
            {
                return Enable();
            }
            return new AnalyzerOperationResult() { Error = false };
        }

        public AnalyzerOperationResult SetAudioDevice(AudioDevice newAudioDevice)
        {
            //TODO: Stop and Re-Sync
            bool wasEnabled = Enabled;
            if (wasEnabled)
            {
                var disableResult = Disable();
                if (disableResult.Error)
                {
                    return disableResult;
                }
            }
            AudioDevice = newAudioDevice;

            if (wasEnabled)
            {
                return Enable();
            }

            return new AnalyzerOperationResult() { Error = false };
        }

        public AnalyzerOperationResult Enable()
        {
            if(Initialized)
            {
                var disableResult = Disable();
                if(disableResult.Error)
                {
                    Enabled = false;
                    return disableResult;
                }
            }

            if(!Initialized)
            {
                if(!IsBASSInitialized)
                {
                    Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_UPDATETHREADS, false);
                    IsBASSInitialized = Bass.BASS_Init(0, 48000, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
                    if(!IsBASSInitialized)
                    {
                        return new AnalyzerOperationResult() { Error = true, Exception = new Exception("Failed to initialize BASS no sound device.") };
                    }
                }

                bool result = BassWasapi.BASS_WASAPI_Init(AudioDevice.DeviceID, 0, 0, BASSWASAPIInit.BASS_WASAPI_BUFFER, 1f, 0.05f, WASAPIPROC, IntPtr.Zero);
                if(!result)
                {
                    var error = Bass.BASS_ErrorGetCode();
                    Disable();
                    return new AnalyzerOperationResult() { Error = true, Exception = new Exception(String.Format("Error Initializing Audio Device: {0}", error)) };
                }

                Initialized = true;
                result = BassWasapi.BASS_WASAPI_Start();
                Enabled = true;

                System.Threading.Thread.Sleep(500);
                dispatchTimer.IsEnabled = true;
                dispatchTimer.Start();

                return new AnalyzerOperationResult() { Error = false };
            }
            return new AnalyzerOperationResult() { Error = true, Exception = new Exception("Enable failed to execute") };
        }
        public AnalyzerOperationResult Disable()
        {
            AnalyzerOperationResult result = new AnalyzerOperationResult();
            dispatchTimer.IsEnabled = false;
            dispatchTimer.Stop();
            if (Initialized)
            {
                try
                {
                    BassWasapi.BASS_WASAPI_Stop(true);
                    BassWasapi.BASS_WASAPI_Free();
                    Bass.BASS_Free();
                    result.Error = false;
                    Initialized = false;
                    Enabled = false;
                    IsBASSInitialized = false;
                }
                catch (Exception ex)
                {
                    result.Error = true;
                    result.Exception = ex;
                }
                
            }
            return result;
        }

        private void DispatchTimer_Tick(object sender, EventArgs e)
        {
            
            if(Enabled && Initialized)
            {
                SpectrumData spectrumData = new SpectrumData();
                int ret = BassWasapi.BASS_WASAPI_GetData(FFT, (int)BASSData.BASS_DATA_FFT2048);
                
                
                if (ret < -1) return;
                int level = BassWasapi.BASS_WASAPI_GetLevel();
                spectrumData.LeftLevel = Utils.LowWord32(level);
                spectrumData.RightLevel = Utils.HighWord32(level);


                DoFFTCalcPeak(spectrumData);


                Messenger.Default.Send(new AnalyzerDataEventArgs() { Data = spectrumData });   
                OnAnalyzerData(this, new AnalyzerDataEventArgs() { Data=spectrumData});
                



            }
        }




        private void DoFFTCalcPeak(SpectrumData spectrumData)
        {
           
            int x, z;
            double w;
            int b0 = 0;
            double factor = 3f;
            for (x = 0; x < SpectrumCount; x++)
            {
                float peak = 0;
                int b1 = (int)Math.Pow(2, x * 10.0 / (SpectrumCount - 1));
                b1 = InRange(b1, b0, b0 + 1, 1023, 1023, true, false);
                for (; b0 < b1; b0++)
                {
                    if (peak < FFT[1 + b0])
                    {
                        peak = FFT[1 + b0];
                        
                    }
                    
                }

                //factor += Math.Pow(x, (double)1 / 6);


                w = (double)(Math.Sqrt(peak) * factor);
                z = InRange((int)(w * 100), 0, 100);
                spectrumData.Spectrum.Add(z);
            }
            
        }


        /*private void DoFFTCalcDb(SpectrumData spectrumData)
        {
            int x, z;
            double w;
            int b0 = 0;
            double factor = 2.5f;
            for (x = 0; x < SpectrumCount; x++)
            {
                double peak = 0;
                int b1 = (int)Math.Pow(2, x * 10.0 / (SpectrumCount - 1));
                int freq = 0;
                int windowCount = 0;
                List<float> window = new List<float>();
                b1 = InRange(b1, b0, b0 + 1, 1023, 1023, true, false);
                for (; b0 < b1; b0++)
                {
                    if (peak < FFT[1 + b0])
                    {
                        peak = FFT[1 + b0];
                        freq = Utils.FFTIndex2Frequency(1 + b0, 1023, 48000);
                        
                    }
                    window.Add(FFT[1 + b0]);
                    windowCount++;
                }
                

                //if (freq > 50 && freq < 560)
                //{
                //    factor = 3.0f;
                //}
                //else if (freq >= 560 && freq < 860)
                //{
                //    factor = 3.5f;
                //}
                //else if (freq >= 860)
                //{
                //    factor = 4.0f;
                //}
                w = (double)(Math.Sqrt(peak) * factor);
                
                w = (double)10 * Math.Log10(peak / 6);
                //z = InRange((int)(w * 100), 0, 100);
                z = (int)w;
                spectrumData.Spectrum.Add(z);
            }
        }*/
        private int InRange(int val, int min, int max)
        {
            if (val < min)
                return min;
            if (val > max)
                return max;
            return val;

        }

        private int InRange(int val, int minThreshold, int minValue, int maxThreshold, int maxValue, bool minInclusive = false, bool maxInclusive = false)
        {
            int retval = val;
            if (minInclusive)
            {
                if (val <= minThreshold)
                    retval = minValue;
            }
            else
            {
                if (val < minThreshold)
                    retval = minValue;
            }

            if (maxInclusive)
            {
                if (val >= maxThreshold)
                    retval = maxValue;

            }
            else
            {
                if (val > maxThreshold)
                    retval = maxValue;
            }

            return retval;

        }

        
        // WASAPI callback, required for continuous recording
        private int WASAPI_Process(IntPtr buffer, int length, IntPtr user)
        {
            return length;
        }

        public void Dispose()
        {
            try
            {
                Disable();
            }
            catch
            {

            }
            
            BassWasapi.BASS_WASAPI_Free();
            Bass.BASS_Free();
        }

        //i - bin to access
        //samples - sampling rate in Hz - 44100,48000,96000
        //nFFT - size of FFT array
        public static double Index2Freq(int i, double samples, int nFFT)
        {
            return (double)i * (samples / nFFT);
        }

        public static int Freq2Index(double freq, double samples, int nFFT)
        {
            return (int)(freq / (samples / nFFT));
        }
    }
}
