﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Drawing;
using System.Windows.Forms;
using LEDControllerClient.Interface;
using System.Windows.Threading;
using LEDControllerClient.Message;
using GalaSoft.MvvmLight.Messaging;

namespace LEDControllerClient.GameEffects
{
    public class ApexLegends : GameEffect
    {

        public ApexLegends()
        {
            _GameName = "Apex Legends";
            _GameExecutable = "r5Apex.exe";
            Setup(354, 2033, 477, 1, 16);
        }
        internal override Color GetColor(Bitmap Image)
        {

           GameEffectMessage msg = new GameEffectMessage();
            msg.BMP = Image;
            int validCount = 0;
            
            for(int i =  0; i < Image.Width; i++)
            {
                Color c = Image.GetPixel(i, 0);
                if(c.R>=200 && c.G >=200 && c.B >=200)
                {
                    validCount++;
                }
                msg.Colors.Add(c);
            }
            double percent = (double)validCount / Image.Width;
            msg.percent = percent;
            if(percent > 0.25)
            {
                msg.DeterminedColor = Color.White;
            }
            else
            {
                msg.DeterminedColor = Color.Red;
            }

            Messenger.Default.Send(msg);
            return msg.DeterminedColor;
      
        }

        
    }
}
