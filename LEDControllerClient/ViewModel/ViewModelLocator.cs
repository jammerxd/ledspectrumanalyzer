/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:LEDControllerClient"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace LEDControllerClient.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<VUMeterViewModel>();
            
            SimpleIoc.Default.Register<AnalyzerViewModel>();
            SimpleIoc.Default.Register<GameEffectViewModel>();
            SimpleIoc.Default.Register<ControllerManagerViewModel>();
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }
        public VUMeterViewModel VUMeter
        {
            get
            {
                return ServiceLocator.Current.GetInstance<VUMeterViewModel>();
            }
        }
        public AnalyzerViewModel Analyzer
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AnalyzerViewModel>();
            }
        }
        public GameEffectViewModel GameEffect
        {
            get
            {
                return ServiceLocator.Current.GetInstance<GameEffectViewModel>();
            }
        }
        public ControllerManagerViewModel ControllerManager
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ControllerManagerViewModel>();
            }
        }
        
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}