﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using LEDControllerClient.Class;
using LEDControllerClient.Message;
using LEDControllerClient.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Un4seen.BassWasapi;

namespace LEDControllerClient.ViewModel
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class AnalyzerViewModel :ViewModelBase
    {
        public ObservableCollection<AudioDevice> AudioDevices { get; set; }
        public Analyzer analyzer { get; set; }


        public bool EnableDisableButtonEnabled { get; set; }
        public string EnableDisableButtonText { get; set; }


        public bool AudioDeviceComboBoxEnabled { get; set; }
        public AudioDevice SelectedAudioDevice { get; set; }

        public bool FrameDelayNumericEnabled { get; set; }
        public int FrameDelay { get; set; }

        public bool SpectrumCountNumericEnabled { get; set; }
        public int SpectrumCount { get; set; }

        public bool EnableVUMeterVisualsChecked { get; set; }

        public AnalyzerViewModel()
        {
            AudioDevices = new ObservableCollection<AudioDevice>();
            analyzer = new Analyzer();
            EnableDisableButtonEnabled = false;
            EnableDisableButtonText = "Enable";
            AudioDeviceComboBoxEnabled = false;
            SpectrumCountNumericEnabled = true;
            FrameDelayNumericEnabled = true;
            LoadAudioDevices();
            FrameDelay = 8;
            SpectrumCount = 7;
            EnableVUMeterVisualsChecked = true;
            EnableVUMeterVisualsCheckedUnchecked();
            
        }

        private void LoadAudioDevices()
        {
            AudioDevices.Clear();

            int count = BassWasapi.BASS_WASAPI_GetDeviceCount();
            for(int i = 0; i < count; i++)
            {
                var device = BassWasapi.BASS_WASAPI_GetDeviceInfo(i);
                if(device.IsEnabled && device.IsLoopback)
                {
                    AudioDevices.Add(new AudioDevice() { DeviceID = i, DisplayName = device.name });
                }
            }

            if(AudioDevices.Count > 0)
            {
                SelectedAudioDevice = AudioDevices[0];
                EnableDisableButtonEnabled = true;
                AudioDeviceComboBoxEnabled = true;
            }
        }


        private RelayCommand _EnableDisableAnalyzerCommand { get; set; }
        public RelayCommand EnableDisableAnalyzerCommand
        {
            get
            {
                if(_EnableDisableAnalyzerCommand == null)
                {
                    _EnableDisableAnalyzerCommand = new RelayCommand(EnableDisableAnalyzer);
                }
                return _EnableDisableAnalyzerCommand;
            }
        }


        private void EnableDisableAnalyzer()
        {
            EnableDisableButtonEnabled = false;
            AudioDeviceComboBoxEnabled = false;
            SpectrumCountNumericEnabled = false;
            FrameDelayNumericEnabled = false;
            
            if (analyzer.Enabled)
            {
                var result = analyzer.Disable();
                if (!result.Error)
                {
                    analyzer.Dispose();
                    analyzer = new Analyzer();
                    
                    AudioDeviceComboBoxEnabled = true;
                    SpectrumCountNumericEnabled = true;
                    FrameDelayNumericEnabled = true;
                    EnableDisableButtonText = "Enable";
                }
                else
                {
                    MessageBox.Show(result.Exception.Message);
                }
            }
            else
            {
                EnableVUMeterVisualsCheckedUnchecked();
                analyzer.SetAudioDevice(SelectedAudioDevice);
                analyzer.SetFrameDelay(FrameDelay);
                analyzer.SetSpectrumCount(SpectrumCount);


                var result = analyzer.Enable();
                if (!result.Error)
                {
                    EnableDisableButtonText = "Disable";
                }
                else
                {
                    MessageBox.Show(result.Exception.Message);
                }
            }
            EnableDisableButtonEnabled = true;
        }

        private RelayCommand _EnableVUMeterVisualsCheckedUncheckedCommand { get; set; }
        public RelayCommand EnableVUMeterVisualsCheckedUncheckedCommand
        {
            get
            {
                if(_EnableVUMeterVisualsCheckedUncheckedCommand == null)
                {
                    _EnableVUMeterVisualsCheckedUncheckedCommand = new RelayCommand(EnableVUMeterVisualsCheckedUnchecked);
                }
                return _EnableVUMeterVisualsCheckedUncheckedCommand;
            }
        }
        private void EnableVUMeterVisualsCheckedUnchecked()
        {
            Messenger.Default.Send(new VUMeterVisualsEnabledChangedMessage() { Enable = EnableVUMeterVisualsChecked });
        }
    }
}
