﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LEDControllerClient.ViewModel
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class ControllerManagerViewModel : ViewModelBase
    {
        public ObservableCollection<Interface.LEDController> Controllers { get; set; }
        public Interface.LEDController SelectedController { get; set; }
        public ControllerManagerViewModel()
        {
            Controllers = new ObservableCollection<Interface.LEDController>();
            var defaultController = new Model.BeagleBoneLEDController("Default BBB", "10.0.0.15", 7890, true, 300, 1);
            defaultController.Connect();



            var l1 = new Model.LIFX.LIFXA19BulbStrobeController("Ceiling Fan Light 1", "10.0.0.244", Lifx.Color.White,0.5f, new Model.LIFX.LIFXA19BulbStrobeController.Configuration[]
            {
                new Model.LIFX.LIFXA19BulbStrobeController.Configuration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 0, StrobeThreshold = 100},
                new Model.LIFX.LIFXA19BulbStrobeController.Configuration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 1, StrobeThreshold = 100},
            });
            var l2 = new Model.LIFX.LIFXA19BulbStrobeController("Ceiling Fan Light 1", "10.0.0.245", Lifx.Color.White, 0.5f, new Model.LIFX.LIFXA19BulbStrobeController.Configuration[]
{
                new Model.LIFX.LIFXA19BulbStrobeController.Configuration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 0, StrobeThreshold = 100},
                new Model.LIFX.LIFXA19BulbStrobeController.Configuration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 1, StrobeThreshold = 100},
});
            var l3 = new Model.LIFX.LIFXA19BulbStrobeController("Ceiling Fan Light 1", "10.0.0.246", Lifx.Color.White, 0.5f, new Model.LIFX.LIFXA19BulbStrobeController.Configuration[]
{
                new Model.LIFX.LIFXA19BulbStrobeController.Configuration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 0, StrobeThreshold = 100},
                new Model.LIFX.LIFXA19BulbStrobeController.Configuration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 1, StrobeThreshold = 100},
});

            Task.Run(async() => await l1.Connect()).Wait();
            Task.Run(async () => await l2.Connect()).Wait();
            Task.Run(async () => await l3.Connect()).Wait();
           
            Controllers.Add(defaultController);
            Controllers.Add(l1);
            Controllers.Add(l2);
            Controllers.Add(l3);

        }
    }
}
