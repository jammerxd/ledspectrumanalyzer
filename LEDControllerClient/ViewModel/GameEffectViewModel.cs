﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using LEDControllerClient.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDControllerClient.ViewModel
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class GameEffectViewModel : ViewModelBase
    {
        public bool IsEnabled { get; set; }
        public bool IsCmbxEnabled { get; set; }

        public GameEffect SelectedEffect { get; set; }
        public int SelectedGameIndex { get; set; }
        public ObservableCollection<GameEffect> GameEffects { get; set; }
        public string DisplayValue { get; set; }

        public GameEffectViewModel()
        {
            GameEffects = new ObservableCollection<GameEffect>();
            GameEffects.Add(new GameEffects.ApexLegends());
            SelectedEffect = GameEffects.ElementAt(0);
            SelectedGameIndex = 0;
            IsEnabled = false;
            IsCmbxEnabled = true;
        }


        private RelayCommand _EnableGameEffectCheckedUncheckedCommand { get; set; }
        public RelayCommand EnableGameEffectCheckedUncheckedCommand
        {
            get
            {
                if (_EnableGameEffectCheckedUncheckedCommand == null)
                {
                    _EnableGameEffectCheckedUncheckedCommand = new RelayCommand(EnableGameEffectCheckedUnchecked);
                }
                return _EnableGameEffectCheckedUncheckedCommand;
            }
        }
        private void EnableGameEffectCheckedUnchecked()
        {
            IsCmbxEnabled = !IsEnabled;
            SelectedEffect.SetEnabled(IsEnabled);
            if(IsEnabled)
            {
                Messenger.Default.Register<Message.GameEffectMessage>(this, GameEffectMessage);
            }
            else
            {
                Messenger.Default.Unregister<Message.GameEffectMessage>(this);
            }
            
        }
        private void GameEffectMessage(Message.GameEffectMessage msg)
        {
            DisplayValue = String.Format("{0:P2}",msg.percent);
        }
    }
}
