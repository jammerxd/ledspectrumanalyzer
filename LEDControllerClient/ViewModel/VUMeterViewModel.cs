﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using LEDControllerClient.Class;
using LEDControllerClient.Message;
using LEDControllerClient.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace LEDControllerClient.ViewModel
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class VUMeterViewModel : ViewModelBase
    {

        public List<int> SpectrumData { get; set; }
        public List<float> SpectrumDataV2 { get; set; }
        public int LeftLevel { get; set; }
        public int RightLevel { get; set; }

        private int lastSpectrumDataCount { get; set; }
        public ObservableCollection<SpectrumBandModel> MetroProgressBars { get; set; }

        private bool DoUpdateMeter { get; set; }
        public VUMeterViewModel()
        {
            
            MetroProgressBars = new ObservableCollection<SpectrumBandModel>();

            Messenger.Default.Register<VUMeterVisualsEnabledChangedMessage>(this, SetEnableDisableVisual);
        }

        public void SetEnableDisableVisual(VUMeterVisualsEnabledChangedMessage message)
        {
            if(message.Enable)
            {
                if(!DoUpdateMeter)
                {
                    DoUpdateMeter = true;
                    Messenger.Default.Register<AnalyzerDataEventArgs>(this, AnalyzerData);
                }
            }
            else
            {
                if(DoUpdateMeter)
                {
                    Messenger.Default.Unregister<AnalyzerDataEventArgs>(this);
                    DoUpdateMeter = false;
                    MetroProgressBars.Clear();
                    LeftLevel = 0;
                    RightLevel = 0;
                    lastSpectrumDataCount = 0;
                   
                }
            }
        }
        private void AnalyzerData(AnalyzerDataEventArgs e)
        {

            SpectrumData = e.Data.Spectrum;
            LeftLevel = e.Data.LeftLevel;
            RightLevel = e.Data.RightLevel;
            if(lastSpectrumDataCount < SpectrumData.Count)
            {
                int diff = SpectrumData.Count - lastSpectrumDataCount;
                for (int i=0;i<diff;i++)
                {
                    int leftPadding = i == 0 ? 0 : 10;
                    var progressBar = new SpectrumBandModel() { SpectrumLevel = SpectrumData[i] };
                    MetroProgressBars.Add(progressBar);
                }
            }
            else if(lastSpectrumDataCount > SpectrumData.Count)
            {
                int diff = lastSpectrumDataCount - SpectrumData.Count;
                for (int i =0; i<diff;i++)
                {
                    MetroProgressBars.RemoveAt(MetroProgressBars.Count - 1);
                }
            }

            for(int i = 0; i < SpectrumData.Count; i++)
            {
                MetroProgressBars[i].SpectrumLevel = SpectrumData[i];
            }
            lastSpectrumDataCount = SpectrumData.Count;
        }

        




    }
}
