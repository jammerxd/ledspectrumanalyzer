﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDControllerClient.Message
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class VUMeterVisualsEnabledChangedMessage
    {
        public bool Enable { get; set; }
    }
}
