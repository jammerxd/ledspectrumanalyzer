﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDControllerClient.Message
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class GameEffectMessage
    {
        public Bitmap BMP { get; set; }
        public double percent { get; set; }
        public ObservableCollection<Color> Colors { get; set; }
        public Color DeterminedColor { get; set; }
        public GameEffectMessage()
        {
            Colors = new ObservableCollection<Color>();
            BMP = new Bitmap(1, 1);
            percent = 0;
            DeterminedColor = Color.Black;
        }

    }
}
