﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;
using System.Windows.Threading;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace LEDControllerClient.Interface
{
    public class GameEffect
    {
        [DllImport("GDI32.dll")]
        public static extern bool BitBlt(int hdcDest, int nXDest, int nYDest,
       int nWidth, int nHeight, int hdcSrc, int nXSrc, int nYSrc, int dwRop);

        [DllImport("GDI32.dll")]
        public static extern int CreateCompatibleBitmap(int hdc, int nWidth, int nHeight);[DllImport("GDI32.dll")]
        public static extern int CreateCompatibleDC(int hdc);

        [DllImport("GDI32.dll")]
        public static extern bool DeleteDC(int hdc);

        [DllImport("GDI32.dll")]
        public static extern bool DeleteObject(int hObject);


        [DllImport("gdi32.dll")]
        static extern int CreateDC(string lpszDriver, string lpszDevice, string lpszOutput, IntPtr lpInitData);

        [DllImport("GDI32.dll")]
        public static extern int GetDeviceCaps(int hdc, int nIndex);

        [DllImport("GDI32.dll")]
        public static extern int SelectObject(int hdc, int hgdiobj);

        [DllImport("User32.dll")]
        public static extern int GetDesktopWindow();

        [DllImport("User32.dll")]
        public static extern int GetWindowDC(int hWnd);

        [DllImport("User32.dll")]
        public static extern int ReleaseDC(int hWnd, int hDC);

        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public int intervalMilliseconds { get; set; }
        public bool Enabled { get;  set; }
        private DispatcherTimer timer { get; set; }
        public bool IsSetup { get; private set; }

        internal string _GameName { get; set; }
        internal string _GameExecutable { get; set; }
        public string GameName {
            get
            {
                return _GameName;
            }
        }
        public string GameExecutable
        {
            get
            {
                return _GameExecutable;
            }
        }
        internal virtual Color GetColor(Bitmap bmp)
        {
            return Color.Black;
        }
        public GameEffect()
        {

        }
   

        internal void Setup(int _x, int _y, int _width, int _height, int _intervalMilliseconds)
        {
            X = _x;
            Y = _y;
            Width = _width;
            Height = _height;
            intervalMilliseconds = _intervalMilliseconds;
            Enabled = false;
            timer = new DispatcherTimer();
            timer.Tick += T_Tick;
            timer.IsEnabled = false;
            timer.Interval = TimeSpan.FromMilliseconds(intervalMilliseconds);
            IsSetup = true;
        }

        private void T_Tick(object sender, EventArgs e)
        {
            int hdcSrc = CreateDC("DISPLAY", null, null, IntPtr.Zero);
            int hdcDest = CreateCompatibleDC(hdcSrc);
            int hBitmap = CreateCompatibleBitmap(hdcSrc, Width, Height);
            SelectObject(hdcDest, hBitmap);

            // set the destination area White - a little complicated
            Bitmap myBMP = new Bitmap(Width, Height);
            

            Bitmap bmp = new Bitmap(Width, Height);
            Image ii = (Image)bmp;
            Graphics gf = Graphics.FromImage(ii);
            IntPtr hdc = gf.GetHdc();
            //use whiteness flag to make destination screen white
            BitBlt(hdcDest, 0, 0, Width, Height, (int)hdc, 0, 0, 0x00FF0062);
            gf.Dispose();
            ii.Dispose();
            bmp.Dispose();

            //Now copy the areas from each screen on the destination hbitmap
            Screen[] screendata = Screen.AllScreens;
            int _internalX, _internalX1, _internalY, _internalY1;
            for (int i = 0; i < screendata.Length; i++)
            {
                if (screendata[i].Bounds.X > (X + Width) || (screendata[i].Bounds.X +
                    screendata[i].Bounds.Width) < X || screendata[i].Bounds.Y > (Y + Height) ||
                    (screendata[i].Bounds.Y + screendata[i].Bounds.Height) < Y)
                {// no common area
                }
                else
                {
                    // something  common
                    if (X < screendata[i].Bounds.X) _internalX = screendata[i].Bounds.X; else _internalX = X;
                    if ((X + Width) > (screendata[i].Bounds.X + screendata[i].Bounds.Width))
                        _internalX1 = screendata[i].Bounds.X + screendata[i].Bounds.Width;
                    else _internalX1 = X + Width;
                    if (Y < screendata[i].Bounds.Y) _internalY = screendata[i].Bounds.Y; else _internalY = Y;
                    if ((Y + Height) > (screendata[i].Bounds.Y + screendata[i].Bounds.Height))
                        _internalY1 = screendata[i].Bounds.Y + screendata[i].Bounds.Height;
                    else _internalY1 = Y + Height;
                    // Main API that does memory data transfer
                    BitBlt(hdcDest, _internalX - X, _internalY - Y, _internalX1 - _internalX, _internalY1 - _internalY, hdcSrc, _internalX, _internalY,
                                0x40000000 | 0x00CC0020); //SRCCOPY AND CAPTUREBLT
                }
            }

            myBMP = Bitmap.FromHbitmap(new IntPtr(hBitmap));
            //myBMP.Save("temp.jpg", ImageFormat.Jpeg);
            GetColor(myBMP);
            DeleteDC(hdcSrc);
            DeleteDC(hdcDest);
            DeleteObject(hBitmap);
            myBMP.Dispose();
        }

        public void SetEnabled(bool enabled)
        {
            Enabled = enabled;
            timer.IsEnabled = enabled;
            if(enabled)
            {
                timer.Start();
            }
            else
            {
                timer.Stop();
            }
        }
        

    
        

    }
}
