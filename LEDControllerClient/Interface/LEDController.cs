﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDControllerClient.Interface
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class LEDController
    {
        public string Name { get; set; }
        public string Type { get; set; }

        public LEDController(string Type, string Name)
        {
            this.Type = Type;
            this.Name = Name;
        }

        
    }
}
