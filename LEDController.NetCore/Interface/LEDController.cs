﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDController.NetCore.Interface
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class LEDController
    {
        public enum Animation
        {
            Default=0,
            MNWild=1,
            USNationalAnthem=2
        }
        public string Name { get; set; }
        public string Type { get; set; }

        public bool Enabled { get; private set; }
        public int CallbackId { get; private set; }
        private Class.Analyzer analyzer { get; set; }
        protected Animation CurrentAnimation { get; set; }
        protected object AnimationLockObj { get; set; }

        public LEDController(string Type, string Name, Class.Analyzer analyzer)
        {
            this.CurrentAnimation = Animation.Default;
            this.AnimationLockObj = new object(); ;
            this.Type = Type;
            this.Name = Name;
            this.Enabled = false;
            this.CallbackId = -1;
            this.analyzer = analyzer;
            
        }

        public virtual async Task AnalyzerData(Model.SpectrumData data)
        {

        }

        public virtual void Enable()
        {
            if(!this.Enabled)
            {
                this.Enabled = true;
                this.CallbackId = analyzer.AsyncEventHandler.AddCallback(AnalyzerData);
            }
        }
        public virtual void Disable()
        {
            if(this.Enabled)
            {
                this.Enabled = false;
                analyzer.AsyncEventHandler.RemoveCallback(this.CallbackId);
                this.CallbackId = -1;
            }
        }

        public virtual void ChangeAnimation(Animation newAnimation)
        {
            lock (this.AnimationLockObj)
            {
                this.CurrentAnimation = newAnimation;
            }
        }

    }
}
