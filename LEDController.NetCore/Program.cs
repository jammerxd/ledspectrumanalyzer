using LEDController.NetCore.Model;

namespace LEDController.NetCore
{
    internal static class Program
    {
        public static LifxNetPlus.LifxClient LifxClient { get; private set; }
        public static MusicProfileConfig MusicProfileConfig { get; private set; }

        static Program()
        {
            LifxClient = LifxNetPlus.LifxClient.CreateAsync().Result;
            MusicProfileConfig = Model.MusicProfileConfig.LoadFromConfig(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "SpectrumProfiles.json"));
        }
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();
            Application.Run(new Form1());
        }
    }
}