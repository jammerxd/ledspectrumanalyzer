﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDController.NetCore.Model
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class SpectrumData
    {
        public int LeftLevel { get; set; }
        public int RightLevel { get; set; }

        public List<int> Spectrum { get; set; }


        

        public SpectrumData()
        {
            Spectrum = new List<int>();
        }

        public SpectrumData(IEnumerable<int> spectrum)
        {
            Spectrum = new List<int>();
            Spectrum.AddRange(spectrum);
        }
    }

    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class SpectrumDataV2
    {
        public int LeftLevel { get; set; }
        public int RightLevel { get; set; }

        public List<float> Spectrum { get; set; }

        public float AveragedVolue { get; set; }
        public float CurrentVolume { get; set; }
        public float SongBeat { get; set; }
        public float VolumeScalar { get; set; }
        public SpectrumDataV2()
        {
            Spectrum = new List<float>();
        }

        public SpectrumDataV2(IEnumerable<float> spectrum)
        {
            Spectrum = new List<float>();
            Spectrum.AddRange(spectrum);
        }
    }
}
