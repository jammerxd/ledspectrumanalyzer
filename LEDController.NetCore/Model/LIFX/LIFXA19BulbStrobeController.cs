﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using LEDController.NetCore.Class;
//using Lifx;
namespace LEDController.NetCore.Model.LIFX
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class LIFXA19BulbStrobeController : Interface.LEDController, IDisposable
    {
        //static LightFactory factory = new LightFactory();
        public LIFXA19BulbStrobeController(string name, string IPAddress,Color color,float strobeBrightness, IEnumerable<Configuration> configs,Class.Analyzer analyzer) : base("LIFX A19 Bulb Strobe",name, analyzer)
        {
            this.IPAddress = System.Net.IPAddress.Parse(IPAddress);
            needsReset = true;
            run = false;
            isReadyForNext = false;
            
            StrobeColor = color;
            foreach(Configuration config in configs)
            {
                if(config.MusicProfile == null)
                {
                    config.MusicProfile = MusicProfileConfig.Profiles.Where(x => x.Name == config.MusicProfileName).FirstOrDefault();
                }
            }
            StrobeBrightness = strobeBrightness;
            Configurations = configs;

            
            
        }

        private void AnalyzerData(AnalyzerDataEventArgs obj)
        {
         
            if (needsReset && run)
            {
                Reset();
            }
            else if(run)
            {
                updateThread_Elapsed(obj.Data);
            }
        }
        private bool areAll100(SpectrumData data)
        {
            bool AreAll100 = true;
            foreach(Configuration config in Configurations)
            {
                if(AreAll100 && config.MusicProfile.GetAdjustedValue(data.Spectrum[config.SpectrumBand]) < config.StrobeThreshold)
                {
                    AreAll100 = false;
                    break;
                }
            }
            return AreAll100;
        }
        
            

        private async void updateThread_Elapsed(SpectrumData data)
        {

            if (!needsReset && run)
            {
                if (isReadyForNext)
                {
                    isReadyForNext = false;

                    try
                    {

                        if (!strobeTriggered && (DateTime.Now - lastStrobeStop).TotalMilliseconds >= 100 && areAll100(data))
                        {
                            strobeTriggered = true;
                            //light.SetBrightnessAsync(StrobeBrightness);
                            lastStrobeStart = DateTime.Now;
                            //strobeTimer.Start();

                        }
                        isReadyForNext = true;
                    }
                    catch
                    {
                        needsReset = true;
                    }
                }

                try
                {
                    if (strobeTriggered && (DateTime.Now - lastStrobeStart).TotalMilliseconds >= 30)
                    {
                        lastStrobeStop = DateTime.Now;
                        strobeTriggered = false;

                    }
                }
                catch
                {
                    needsReset = true;
                }
            }

            

        }

        public async Task Reset()
        {
            //strobeTimer.Stop();
            //strobeTimer.Enabled = false;
            isReadyForNext = false;
            run = false;
            Disconnect();
            await Connect();
        }
        /*public bool CompareColor(Color A, Color B)
        {
            return A.Hue == B.Hue && A.Saturation == B.Saturation;
        }*/
        public async Task Connect()
        {
            //strobeTimer.Stop();
            //strobeTimer.Enabled = false;
            try
            {
                /*if (factory != null)
                {
                    light = await factory.CreateLightAsync(this.IPAddress);
                    await light.SetPowerAsync(Power.On);
                    
                    await light.SetColorAsync(StrobeColor, 0);
                    if(CompareColor(StrobeColor,Color.White))
                    {
                        await light.SetTemperatureAsync(Temperature.BlueDaylight);
                    }
                    await light.SetBrightnessAsync(Percentage.MinValue);


                    //strobeTimer.Enabled = true;

                    lastStrobeStart = DateTime.Now;


                }*/
                needsReset = false;
                run = true;
                isReadyForNext = true;
            }
            catch
            {
                needsReset = true;
                run = false;
                isReadyForNext = false;
            }
        }
        public void Disconnect()
        {
            try
            {
                /*if (light != null)
                {
                    try
                    {
                        light.SetBrightnessAsync(Percentage.MinValue).Wait();
                    }
                    catch
                    {

                    }
                    using (light) { }
                }*/
            }
            catch
            {

            }
            finally
            {
                needsReset = true;
                isReadyForNext = false;
                run = false;
            }
        }
        public void Dispose()
        {
            run = false;
            //strobeTimer.Enabled = false;
            //strobeTimer.Stop();
            //strobeTimer.Dispose();
            Disconnect();
        }
        //private ILight light;
        private volatile bool run;
        private volatile bool needsReset;
        private volatile bool isReadyForNext;
        private System.Net.IPAddress IPAddress;
        private volatile bool strobeTriggered = false;

        private MusicProfileConfig MusicProfileConfig = Model.MusicProfileConfig.LoadFromConfig(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "SpectrumProfiles.json"));
        private DateTime lastStrobeStart = DateTime.Now;
        private DateTime lastStrobeStop = DateTime.Now;
        private IEnumerable<Configuration> Configurations;
        private Color StrobeColor;
        private float StrobeBrightness;
        //private Timer strobeTimer;



        public class Configuration
        {
            public Configuration()
            {

            }
            public MusicProfile MusicProfile { get; set; }
            public int SpectrumBand { get; set; }
            public float Multiplier { get; set; }
            public int StrobeThreshold { get; set; }
            public string MusicProfileName { get; set; }

            
        }
    }


}
