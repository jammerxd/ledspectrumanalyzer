﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDController.NetCore.Model
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class SpectrumBandModel
    {
        public int SpectrumLevel { get; set; }
    }

    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class SpectrumBandModelV2
    {
        public float SpectrumLevel { get; set; }
    }
}
