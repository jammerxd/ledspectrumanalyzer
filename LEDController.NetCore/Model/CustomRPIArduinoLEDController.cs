﻿using LEDController.NetCore.Class;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDController.NetCore.Model
{
    public class CustomRPIArduinoLEDController:Interface.LEDController
    {
        public CustomRPIArduinoLEDController(string Name, string IPAddress, int Port, bool UseUDP, int Count, int OPCChannel,Class.Analyzer analyzer) : base("Custom RPI Arduino", Name,analyzer)
        {

        }


        public string IPAddress { get; set; }
        public int Port { get; set; }
        public bool UseUDP { get; set; }
        public int PixelCount { get; set; }

        public LEDMusicChannel RedChannel { get; set; }
        public LEDMusicChannel GreenChannel { get; set; }
        public LEDMusicChannel BlueChannel { get; set; }
        public int OPCChannel { get; set; }
        public ObservableCollection<string> AnimationTypes { get; set; }
        public String SelectedAnimationType { get; set; }



        public ObservableCollection<System.Drawing.Color> Pixels { get; set; }
        private AsyncTCP.Client.AsynchronousClient client;
        private Stopwatch stopWatch;

        private int updateLEDs = 3;
        private int section_sz;
        private System.Drawing.Color last_color;
        private MusicProfileConfig MusicProfileConfig = Model.MusicProfileConfig.LoadFromConfig(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "SpectrumProfiles.json"));


        public void Connect()
        {
            this.client.Connect(this.IPAddress, this.Port, this.UseUDP ? System.Net.Sockets.ProtocolType.Udp : System.Net.Sockets.ProtocolType.Tcp);
        }
        public void Disconnect()
        {
            this.client.Disconnect();
        }
    }
}
