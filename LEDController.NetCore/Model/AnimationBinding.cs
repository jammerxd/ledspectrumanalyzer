﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDController.NetCore.Model
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class AnimationBinding
    {
        public AnimationBinding()
        {
            this.Name = "Default";
            this.Animation = Interface.LEDController.Animation.Default;
        }
        public AnimationBinding(string name, Interface.LEDController.Animation animation)
        {
            this.Name = name;
            this.Animation = animation;
        }
        public string Name { get; private set; }
        public Interface.LEDController.Animation Animation { get; private set; }
    }
}
