﻿using LEDController.NetCore.Class;
using LifxNetPlus;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace LEDController.NetCore.Model
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class LIFXPlusStrobeController : Interface.LEDController,IDisposable
    {
        public LIFXPlusStrobeController(string name,string macAddress, IEnumerable<SpectrumConfiguration> spectrumConfigurations,StrobeConfiguration strobeConfiguration, Class.Analyzer analyzer) : base("LIFX Plus A19 Bulb Strobe", name, analyzer)
        {
            needsReset = false;
            MacAddress = macAddress;
            foundDevice = false;
            FoundDeviceTimer = new Timer(15000);
            FoundDeviceTimer.AutoReset = false;
            FoundDeviceTimer.Elapsed += FoundDeviceTimer_Elapsed;
            FoundDeviceTimer.Enabled = true;
            
#pragma warning disable CS8622 // Nullability of reference types in type of parameter doesn't match the target delegate (possibly because of nullability attributes).
            Program.LifxClient.DeviceDiscovered += ClientDeviceDiscovered;
            Program.LifxClient.DeviceLost += ClientDeviceLost;
#pragma warning restore CS8622 // Nullability of reference types in type of parameter doesn't match the target delegate (possibly because of nullability attributes).
            Program.LifxClient.StartDeviceDiscovery();
            FoundDeviceTimer.Start();
            SpectrumConfigurations = spectrumConfigurations;
            StrobeConfig = strobeConfiguration;

            foreach (SpectrumConfiguration config in SpectrumConfigurations)
            {
                if (config.MusicProfile == null)
                {
                    config.MusicProfile = Program.MusicProfileConfig.Profiles.Where(x => x.Name == config.MusicProfileName).FirstOrDefault();
                }
            }

            StrobeTimer = new Timer(StrobeConfig.StrobeDuration);
            StrobeTimer.AutoReset = false;
            StrobeTimer.Enabled = false;
            StrobeTimer.Elapsed += StrobeTimer_Elapsed;
            StrobeTimer.Stop();
            StrobeTimer.Enabled = true;
            
        }

        private async void StrobeTimer_Elapsed(object? sender, ElapsedEventArgs e)
        {
            if (!needsReset && foundDevice)
            {
                Interface.LEDController.Animation animation = Animation.Default;
                lock(this.AnimationLockObj)
                {
                    animation = this.CurrentAnimation;
                }

                if(animation == Animation.MNWild)
                {
                    this.StrobeDelayMNWildAnimation = rand.Next(20, 200);
                }
                await Program.LifxClient.SetColorAsync(light, StrobeConfig.OffColor);
                lastStrobeStop = DateTime.Now;
                strobeTriggered = false;
            }

        }

        private void FoundDeviceTimer_Elapsed(object? sender, ElapsedEventArgs e)
        {
            if(!foundDevice)
            {
                Program.LifxClient.StopDeviceDiscovery();
            }
        }

        public override async Task AnalyzerData(Model.SpectrumData data)
        {
            if(!needsReset && foundDevice)
            {
                Animation animation = Animation.Default;
                lock (this.AnimationLockObj)
                {
                    animation = this.CurrentAnimation;
                }
                if(animation == Animation.MNWild)
                {
                    if (!strobeTriggered && (DateTime.Now - lastStrobeStop).TotalMilliseconds >= this.StrobeDelayMNWildAnimation)
                    {
                        strobeTriggered = true;
                        int colorDetermine = rand.Next(0, 3);
                        var color = StrobeConfig.StrobeColor;
                        if (colorDetermine == 0)
                        {
                            color = new LifxColor(0.0, 1.0, 1.0, 9000);//red
                        }
                        else if (colorDetermine == 1)
                        {
                            color = new LifxColor(120, 1.0, 1.0, 9000);//green
                        }
                        else if (colorDetermine == 2)
                        {
                            color = new LifxColor(0, 0.0, 1.0, 9000);//white
                        }
                        await Program.LifxClient.SetColorAsync(light, color);
                        lastStrobeStart = DateTime.Now;
                        StrobeTimer.Start();
                    }
                }
                else if(animation == Animation.USNationalAnthem)
                {

                }
                else
                {
                    if (!strobeTriggered && (DateTime.Now - lastStrobeStop).TotalMilliseconds >= StrobeConfig.StrobeBreakDuration && AreAllAtThreshold(data))
                    {
                        strobeTriggered = true;
                        await Program.LifxClient.SetColorAsync(light, StrobeConfig.StrobeColor);
                        lastStrobeStart = DateTime.Now;
                        StrobeTimer.Start();
                    }
                }
                
               
               
            }
            
        }

        public override void ChangeAnimation(Animation newAnimation)
        {
            base.ChangeAnimation(newAnimation);
        }
        public void Dispose()
        {
            
        }


        private void ClientDeviceLost(object sender, LifxClient.DeviceDiscoveryEventArgs e)
        {
            needsReset = true;
            foundDevice = false;
#pragma warning disable CS8622 // Nullability of reference types in type of parameter doesn't match the target delegate (possibly because of nullability attributes).
            Program.LifxClient.DeviceDiscovered += ClientDeviceDiscovered;
#pragma warning restore CS8622 // Nullability of reference types in type of parameter doesn't match the target delegate (possibly because of nullability attributes).
            Program.LifxClient.StartDeviceDiscovery();
        }
        private async void ClientDeviceDiscovered(object sender, LifxClient.DeviceDiscoveryEventArgs e)
        {
            var version = await Program.LifxClient.GetDeviceVersionAsync(e.Device);
            if(e.Device.MacAddressName == MacAddress && !foundDevice)
            {
                light = e.Device;
                Program.LifxClient.StopDeviceDiscovery();
#pragma warning disable CS8622 // Nullability of reference types in type of parameter doesn't match the target delegate (possibly because of nullability attributes).
                Program.LifxClient.DeviceDiscovered -= ClientDeviceDiscovered;
#pragma warning restore CS8622 // Nullability of reference types in type of parameter doesn't match the target delegate (possibly because of nullability attributes).
                foundDevice = true;

                await Program.LifxClient.SetLightPowerAsync(light, true);
               
                await Program.LifxClient.SetColorAsync(light,StrobeConfig.OffColor);
                await Task.Delay(1000);
                await Program.LifxClient.SetColorAsync(light, StrobeConfig.StrobeColor);
                await Task.Delay(1000);
                await Program.LifxClient.SetColorAsync(light, StrobeConfig.OffColor);

                needsReset = false;
            }
        }

        private bool AreAllAtThreshold(SpectrumData data)
        {
            bool ThresholdsMet = true;
            foreach (SpectrumConfiguration config in SpectrumConfigurations)
            {
                if (ThresholdsMet && config.MusicProfile.GetAdjustedValue(data.Spectrum[config.SpectrumBand]) < config.StrobeThreshold)
                {
                    ThresholdsMet = false;
                    break;
                }
            }
            return ThresholdsMet;
        }

        private string MacAddress;
        private Device light;
        private volatile bool foundDevice;
        private Timer FoundDeviceTimer;

        private DateTime lastStrobeStart = DateTime.Now;
        private DateTime lastStrobeStop = DateTime.Now;
        private IEnumerable<SpectrumConfiguration> SpectrumConfigurations;
        private StrobeConfiguration StrobeConfig;
        private Timer StrobeTimer;

        bool needsReset;
        bool strobeTriggered;
        private Random rand = new Random();
        private int StrobeDelayMNWildAnimation;

        public class SpectrumConfiguration
        {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
            public SpectrumConfiguration()
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
            {
                MusicProfile = null;
                SpectrumBand = 0;
                Multiplier = 0.00f;
                StrobeThreshold = int.MaxValue;
                MusicProfileName = "NOT INITIALIZED";
            }
            public MusicProfile MusicProfile { get; set; }
            public int SpectrumBand { get; set; }
            public float Multiplier { get; set; }
            public int StrobeThreshold { get; set; }
            public string MusicProfileName { get; set; }


        }
        public class StrobeConfiguration
        {
            public StrobeConfiguration()
            {
                StrobeBreakDuration = 0;
                StrobeBrightness = 0;
                StrobeColor = new LifxColor(0.0,0.0,1.0,9000);
                OffColor = new LifxColor(0.0, 0.0, 0.0,9000);
                StrobeDuration = 0;
            }

            public int StrobeDuration { get; set; }
            public LifxColor StrobeColor { get; set; }
            public LifxColor OffColor { get; set; }
            public ushort StrobeBrightness { get; set; }
            public int StrobeBreakDuration { get; set; }
        }
        

    }
}
