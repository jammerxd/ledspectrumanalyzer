using LifxNetPlus;
using System.Collections.ObjectModel;
using Un4seen.Bass;
using Un4seen.BassWasapi;

namespace LEDController.NetCore
{
    public partial class Form1 : Form
    {
        private Class.Analyzer analyzer;
        


        private ObservableCollection<Model.AudioDevice> audioDevices;
        private ObservableCollection<Model.AnimationBinding> animations;
        private BindingSource cmbBxDevicesBindingSource;
        private BindingSource cmbBxControllersBindingSource;
        private BindingSource cmbBxAnimationBindingSource;

        private ObservableCollection<Interface.LEDController> Controllers;
        public Form1()
        {
            InitializeComponent();

            
            Controllers = new ObservableCollection<Interface.LEDController>();
            audioDevices = new ObservableCollection<Model.AudioDevice>();
            animations = new ObservableCollection<Model.AnimationBinding>();

            
            cmbBxDevicesBindingSource = new BindingSource();
            cmbBxDevicesBindingSource.DataSource = audioDevices;

            cmbBxAnimationBindingSource = new BindingSource();
            cmbBxAnimationBindingSource.DataSource = animations;

            cmbBxControllersBindingSource = new BindingSource();
            cmbBxControllersBindingSource.DataSource = Controllers;
            
            cmbBxDevicesBindingSource.CurrentItemChanged += CmbBxDevicesBindingSource_CurrentItemChanged;
            cmbBxDevices.DataSource = cmbBxDevicesBindingSource; 
            cmbBxDevices.DisplayMember = "DisplayName";
            cmbBxDevices.Enabled = false;

            cmbBxControllersBindingSource.CurrentItemChanged += CmbBxControllersBindingSource_CurrentItemChanged;
            cmbBxControllers.DataSource = cmbBxControllersBindingSource;
            cmbBxControllers.DisplayMember = "Name";
            cmbBxControllers.Enabled = false;

            cmbBxAnimationBindingSource.CurrentItemChanged += CmbBxAnimationBindingSource_CurrentChanged;
            cmbBxAnimation.DataSource = cmbBxAnimationBindingSource;
            cmbBxAnimation.DisplayMember = "Name";
            cmbBxAnimation.Enabled = false;

            cmbBxAnimationBindingSource.Add(new Model.AnimationBinding("Default",Interface.LEDController.Animation.Default));
            cmbBxAnimationBindingSource.Add(new Model.AnimationBinding("MN Wild", Interface.LEDController.Animation.MNWild));
            cmbBxAnimationBindingSource.Add(new Model.AnimationBinding("US National Anthem", Interface.LEDController.Animation.USNationalAnthem));
            cmbBxAnimation.Enabled = true;

            analyzer = new Class.Analyzer();
            analyzer.SetFrameDelay(16);
            analyzer.SetSpectrumCount(7);

            btnRescanDevices_Click(this, null);

            var defaultController = new Model.BeagleBoneLEDController("Default BBB", "10.0.0.15", 7890, true, 300, 1,analyzer);
            defaultController.Connect();

            var lifxController = new Model.LIFXPlusStrobeController("Default LIFX Plus", "D0:73:D5:67:92:A8",
                new Model.LIFXPlusStrobeController.SpectrumConfiguration[]{
                      new Model.LIFXPlusStrobeController.SpectrumConfiguration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 0, StrobeThreshold = 100},
                      new Model.LIFXPlusStrobeController.SpectrumConfiguration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 1, StrobeThreshold = 100},
                },
                new Model.LIFXPlusStrobeController.StrobeConfiguration() {
                    StrobeBreakDuration = 50,
                    StrobeBrightness = 255,
                    StrobeColor = new LifxColor(0.0, 0.0, 1.0, 9000),
                    OffColor = new LifxColor(0.0, 0.0, 0.0, 9000),
                    StrobeDuration = 20
                },
                analyzer
            );

            var lifxController2 = new Model.LIFXPlusStrobeController("Default LIFX Plus 2", "D0:73:D5:66:17:26",
                new Model.LIFXPlusStrobeController.SpectrumConfiguration[]{
                      new Model.LIFXPlusStrobeController.SpectrumConfiguration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 0, StrobeThreshold = 100},
                      new Model.LIFXPlusStrobeController.SpectrumConfiguration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 1, StrobeThreshold = 100},
                },
                new Model.LIFXPlusStrobeController.StrobeConfiguration()
                {
                    StrobeBreakDuration = 50,
                    StrobeBrightness = 255,
                    StrobeColor = new LifxColor(0.0, 0.0, 1.0, 9000),
                    OffColor = new LifxColor(0.0, 0.0, 0.0, 9000),
                    StrobeDuration = 20
                },
                analyzer
            );

            var lifxController3 = new Model.LIFXPlusStrobeController("Default LIFX Plus 3", "D0:73:D5:66:3D:AC",
                new Model.LIFXPlusStrobeController.SpectrumConfiguration[]{
                      new Model.LIFXPlusStrobeController.SpectrumConfiguration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 0, StrobeThreshold = 100},
                      new Model.LIFXPlusStrobeController.SpectrumConfiguration(){Multiplier = 1.0f, MusicProfileName = "Peaks-Only-Aggressive", SpectrumBand = 1, StrobeThreshold = 100},
                },
                new Model.LIFXPlusStrobeController.StrobeConfiguration()
                {
                    StrobeBreakDuration = 50,
                    StrobeBrightness = 255,
                    StrobeColor = new LifxColor(0.0, 0.0, 1.0, 9000),
                    OffColor = new LifxColor(0.0, 0.0, 0.0, 9000),
                    StrobeDuration = 20
                },
                analyzer
            );

            defaultController.Enable();
            lifxController.Enable();
            lifxController2.Enable();
            lifxController3.Enable();

            cmbBxControllersBindingSource.Add(defaultController);
            cmbBxControllersBindingSource.Add(lifxController);
            cmbBxControllersBindingSource.Add(lifxController2);
            cmbBxControllersBindingSource.Add(lifxController3);

            cmbBxControllers.Enabled = cmbBxControllersBindingSource.Count > 0;

            
        }

        private void CmbBxAnimationBindingSource_CurrentChanged(object? sender, EventArgs e)
        {
            if(cmbBxAnimationBindingSource.Current is Model.AnimationBinding)
            {
                Model.AnimationBinding selected = (Model.AnimationBinding)cmbBxAnimationBindingSource.Current;
                Parallel.ForEach((IEnumerable<Interface.LEDController>)cmbBxControllersBindingSource.List, item => {
                    item.ChangeAnimation(selected.Animation);
                });
            }
        }

        private void CmbBxControllersBindingSource_CurrentItemChanged(object? sender, EventArgs e)
        {
            if(cmbBxControllersBindingSource.Current is Interface.LEDController)
            {
                Interface.LEDController selected = (Interface.LEDController)cmbBxControllersBindingSource.Current;
                if(selected.Enabled)
                {
                    btnSelectedControllerEnable.Text = "Disable";
                }
                else
                {
                    btnSelectedControllerEnable.Text = "Enable";
                }
            }
        }

        private void CmbBxDevicesBindingSource_CurrentItemChanged(object? sender, EventArgs e)
        {

            if (cmbBxDevicesBindingSource.Current is Model.AudioDevice)
            {
                Model.AudioDevice selected = (Model.AudioDevice)cmbBxDevicesBindingSource.Current;
                bool wasEnabled = analyzer.Enabled;
                if(analyzer.Enabled)
                {
                    analyzer.Disable();
                }
                analyzer.SetAudioDevice(selected);
                if (wasEnabled)
                {
                    analyzer.Enable();
                }
            }
        }

        public IEnumerable<Model.AudioDevice> ScanForAudioDevices()
        {
            List<Model.AudioDevice> devices = new List<Model.AudioDevice>();
            try
            {
                int count = BassWasapi.BASS_WASAPI_GetDeviceCount();
                for (int i = 0; i < count; i++)
                {
                    var device = BassWasapi.BASS_WASAPI_GetDeviceInfo(i);
                    if (device.IsEnabled && device.IsLoopback)
                    {
                        devices.Add(new Model.AudioDevice() { DeviceID = i, DisplayName = device.name });
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return devices;
        }

        private void btnRescanDevices_Click(object sender, EventArgs e)
        {
            int initialIndex = 0;
            bool found = false;
            cmbBxDevicesBindingSource.SuspendBinding();
            
            cmbBxDevicesBindingSource.Clear();
            ScanForAudioDevices().ToList().ForEach(x =>
            {
                cmbBxDevicesBindingSource.Add(x);
                if (x.DisplayName != "Music (TC-HELICON GoXLR)" && !found)
                {
                    initialIndex++;
                }
                else if (x.DisplayName == "Music (TC-HELICON GoXLR)")
                {
                    found = true;
                }

            });
            if(initialIndex >= cmbBxDevicesBindingSource.Count)
            {
                initialIndex = 0;
            }
            cmbBxDevicesBindingSource.ResumeBinding();
            cmbBxDevices.Enabled = cmbBxDevicesBindingSource.Count > 0;
            if (cmbBxDevices.Enabled)
            {
                cmbBxDevices.SelectedIndex = initialIndex;
                cmbBxDevicesBindingSource.Position = initialIndex;
                CmbBxDevicesBindingSource_CurrentItemChanged(this, null);
            }
            
        }

        private void btnAnalyzerEnable_Click(object sender, EventArgs e)
        {
            if(btnAnalyzerEnable.Text == "Enable")
            {
                btnAnalyzerEnable.Text = "Disable";
                analyzer.Enable();
            }

            else if(btnAnalyzerEnable.Text == "Disable")
            {
                btnAnalyzerEnable.Text = "Enable";
                analyzer.Disable();
            }
        }

        private void btnSelectedControllerEnable_Click(object sender, EventArgs e)
        {
            if(btnSelectedControllerEnable.Text == "Enable")
            {
                btnSelectedControllerEnable.Text = "Disable";
                if (cmbBxControllersBindingSource.Current is Interface.LEDController)
                {
                    Interface.LEDController selected = (Interface.LEDController)cmbBxControllersBindingSource.Current;
                    selected.Enable();
                }
            }
            else if(btnSelectedControllerEnable.Text == "Disable")
            {
                btnSelectedControllerEnable.Text = "Enable";
                if (cmbBxControllersBindingSource.Current is Interface.LEDController)
                {
                    Interface.LEDController selected = (Interface.LEDController)cmbBxControllersBindingSource.Current;
                    selected.Disable();
                }
            }
        }
    }
}