﻿namespace LEDController.NetCore
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbBxDevices = new System.Windows.Forms.ComboBox();
            this.btnRescanDevices = new System.Windows.Forms.Button();
            this.btnAnalyzerEnable = new System.Windows.Forms.Button();
            this.btnSelectedControllerEnable = new System.Windows.Forms.Button();
            this.cmbBxControllers = new System.Windows.Forms.ComboBox();
            this.cmbBxAnimation = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cmbBxDevices
            // 
            this.cmbBxDevices.FormattingEnabled = true;
            this.cmbBxDevices.Location = new System.Drawing.Point(12, 12);
            this.cmbBxDevices.Name = "cmbBxDevices";
            this.cmbBxDevices.Size = new System.Drawing.Size(339, 23);
            this.cmbBxDevices.TabIndex = 0;
            // 
            // btnRescanDevices
            // 
            this.btnRescanDevices.Location = new System.Drawing.Point(357, 12);
            this.btnRescanDevices.Name = "btnRescanDevices";
            this.btnRescanDevices.Size = new System.Drawing.Size(95, 23);
            this.btnRescanDevices.TabIndex = 1;
            this.btnRescanDevices.Text = "Rescan Devices";
            this.btnRescanDevices.UseVisualStyleBackColor = true;
            this.btnRescanDevices.Click += new System.EventHandler(this.btnRescanDevices_Click);
            // 
            // btnAnalyzerEnable
            // 
            this.btnAnalyzerEnable.Location = new System.Drawing.Point(12, 41);
            this.btnAnalyzerEnable.Name = "btnAnalyzerEnable";
            this.btnAnalyzerEnable.Size = new System.Drawing.Size(75, 23);
            this.btnAnalyzerEnable.TabIndex = 2;
            this.btnAnalyzerEnable.Text = "Enable";
            this.btnAnalyzerEnable.UseVisualStyleBackColor = true;
            this.btnAnalyzerEnable.Click += new System.EventHandler(this.btnAnalyzerEnable_Click);
            // 
            // btnSelectedControllerEnable
            // 
            this.btnSelectedControllerEnable.Location = new System.Drawing.Point(12, 415);
            this.btnSelectedControllerEnable.Name = "btnSelectedControllerEnable";
            this.btnSelectedControllerEnable.Size = new System.Drawing.Size(75, 23);
            this.btnSelectedControllerEnable.TabIndex = 3;
            this.btnSelectedControllerEnable.Text = "Enable";
            this.btnSelectedControllerEnable.UseVisualStyleBackColor = true;
            this.btnSelectedControllerEnable.Click += new System.EventHandler(this.btnSelectedControllerEnable_Click);
            // 
            // cmbBxControllers
            // 
            this.cmbBxControllers.FormattingEnabled = true;
            this.cmbBxControllers.Location = new System.Drawing.Point(12, 386);
            this.cmbBxControllers.Name = "cmbBxControllers";
            this.cmbBxControllers.Size = new System.Drawing.Size(121, 23);
            this.cmbBxControllers.TabIndex = 4;
            // 
            // cmbBxAnimation
            // 
            this.cmbBxAnimation.FormattingEnabled = true;
            this.cmbBxAnimation.Location = new System.Drawing.Point(12, 70);
            this.cmbBxAnimation.Name = "cmbBxAnimation";
            this.cmbBxAnimation.Size = new System.Drawing.Size(339, 23);
            this.cmbBxAnimation.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cmbBxAnimation);
            this.Controls.Add(this.cmbBxControllers);
            this.Controls.Add(this.btnSelectedControllerEnable);
            this.Controls.Add(this.btnAnalyzerEnable);
            this.Controls.Add(this.btnRescanDevices);
            this.Controls.Add(this.cmbBxDevices);
            this.Name = "Form1";
            this.Text = "LED Controller";
            this.ResumeLayout(false);

        }

        #endregion

        private ComboBox cmbBxDevices;
        private Button btnRescanDevices;
        private Button btnAnalyzerEnable;
        private Button btnSelectedControllerEnable;
        private ComboBox cmbBxControllers;
        private ComboBox cmbBxAnimation;
    }
}