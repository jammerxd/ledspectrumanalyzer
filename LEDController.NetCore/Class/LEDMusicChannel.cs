﻿using LEDController.NetCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDController.NetCore.Class
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class LEDMusicChannel
    {
        public LEDMusicChannel()
        {
            SelectedMusicPorfile = new MusicProfile();
        }

        public MusicProfile SelectedMusicPorfile { get; set; }
        public int AudioSpectrumBandIndex { get; set; }
        public float Multiplier { get; set; }
       
    }

}
