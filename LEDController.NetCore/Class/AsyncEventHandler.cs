﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEDController.NetCore.Class
{
    public class AsyncEventHandler<T>
    {
        public AsyncEventHandler()
        {

            Callbacks = new Dictionary<int, Func<T,Task>>();
            LockObj = new object();
        }
        public int AddCallback(Func<T,Task> t)
        {
            int tId = -1;
            try
            {
                lock (LockObj)
                {
                    tId = id++;
                    Callbacks.Add(tId, t);
                }
            }
            catch { }
            return tId;
        }
        public bool RemoveCallback(int tId)
        {
            try
            {
                lock (LockObj)
                {
                    if (Callbacks.ContainsKey(tId))
                    {
                        Callbacks.Remove(tId);
                        return true;
                    }
                }
            } catch { }
            return false;
        }

        public void RunCallbacks(T data)
        {
            try
            {
                lock (LockObj)
                {
                    Parallel.ForEachAsync(Callbacks.Values,async(item,token) =>
                    {
                        if(!token.IsCancellationRequested)
                        {
                            await item(data);
                        }
                    });
                }
            } catch { }
        }
        static AsyncEventHandler()
        {
            id = 1;
        }
        private static int id;
        private Dictionary<int, Func<T, Task>> Callbacks { get; set; }
        private object LockObj;

        
    }
}
