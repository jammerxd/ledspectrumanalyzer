#!/usr/bin/python3
import time
import board
import neopixel
import busio
import adafruit_pca9685

import sys
import socket
import threading
import selectors
import types
import traceback

from Class.LEDColor import LEDColor
from Class.TCPServer import MyTCPServer
from Func.get_ints import convert_data_to_ints
from Func.get_ints import INT_SIZE

i2c = busio.I2C(board.SCL, board.SDA)
pca = adafruit_pca9685.PCA9685(i2c)
pca.frequency = 490
MAX_DUTY_CYCLE=60000
MIN_DUTY_CYCLE=0
ADDRESSABLE_MODE=1
ADDRESSABLE_COUNT=300
ADDRESSABLE_PIN=board.D10
ADDRESSABLE_ORDER=neopixel.RGB
#pixels = neopixel.NeoPixel(board.D18,ADDRESSABLE_COUNT,brightness=0.2,auto_write=False,pixel_order=ADDRESSABLE_ORDER)


def main():
    #global pixels
    #pixels = neopixel.NeoPixel(ADDRESSABLE_PIN,ADDRESSABLE_COUNT,brightness=0.2,auto_write=False,pixel_order=ADDRESSABLE_ORDER)
    server = MyTCPServer("0.0.0.0",2222)

    #server.CompleteData+= CompletedData
    server_thread = threading.Thread(target=server.Listen)
    server_thread.daemon = True
    server_thread.start()
    print("Listening on port 2222")
    print("Press enter to exit")
    input()
    print("Shutting down...")
    server.Shutdown()
    print("Shut down...bye!")

#def update_pixels():
#    global pixels,next_valu
#    for i in range(ADDRESSABLE_COUNT-1,0,-1):
#        if(i > 0):
#            pixels[i]=pixels[i-1]
#    pixels[0] = (0,0,0)
#    time.sleep(0.001)
if __name__ == "__main__":
    main()