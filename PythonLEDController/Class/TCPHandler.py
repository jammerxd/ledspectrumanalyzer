import socket
import threading
import socketserver
from Class.StateObject import StateObject


class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):

    StateObjects = {}

    #def __init__(self,request, client_address, server):
    #    super().__init__(request,client_address,request)

    def setup(self):
        self.StateObj = StateObject()
        print("{}".format(self.StateObj.bufferSize))
        
    def handle(self):

        try:

            while True:
                try:
                    raw_data = self.request.recv(self.StateObj.bufferSize).strip()
                    data = str(raw_data, 'ascii')
                    if(len(data) != 0):
                        print(raw_data)
                    #print("RECEIVED DATA: {}".format(data) )

                except Exception as e:
                    print("ER IN LOOP [{}]: {}".format(self.client_address[0], e))

            print("broke loop")
        except Exception as ex:
            print("ER [{}]: {}".format(self.client_address[0], ex))

        print("DC [{}]: disconnected".format(self.client_address[0]))
 