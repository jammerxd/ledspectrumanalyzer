import sys
import socket
import selectors
import types
import threading
import traceback
import ctypes
from Func.get_ints import convert_data_to_ints
from Func.get_ints import INT_SIZE

from Class.StateObject import StateObject


import time
import board
import neopixel
import busio
import adafruit_pca9685
import numpy as np
from Class.LEDColor import LEDColor



##########################NEW IMPLEMENTATION

i2c = busio.I2C(board.SCL, board.SDA)
pca = adafruit_pca9685.PCA9685(i2c)
pca.frequency = 490
MAX_DUTY_CYCLE=65535
MIN_DUTY_CYCLE=0
ADDRESSABLE_MODE=1
ADDRESSABLE_COUNT=300
ADDRESSABLE_PIN=board.D10
ADDRESSABLE_ORDER=neopixel.RGB
LEDBuffer = [0]*ADDRESSABLE_COUNT

class MyTCPServer:


    def __init__(self,host,port):
        self.host=host
        self.port=port
        self.sel = selectors.DefaultSelector()
        self.lsock =None
        self.mapping= [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 100, 100, 100, 100, 100 ]
        self.pixels=neopixel.NeoPixel(ADDRESSABLE_PIN,ADDRESSABLE_COUNT,auto_write=False,pixel_order=ADDRESSABLE_ORDER)
        #self.CompleteData = Event(self)
        self.pixels.fill((255,255,255))
        self.pixels.show()
        time.sleep(5)
        self.pixels.fill((0,0,0))
        self.pixels.show()
        self.shutdown=False
        self.spectrum_mappings={}
        self.last_spectrum={}
        self.last_spectrum_sizes={}
        self.last_spectrum_count=0
        

    def Listen(self):
        self.lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.lsock.bind((self.host,self.port))
        self.lsock.listen()
        self.lsock.setblocking(False)
        self.sel.register(self.lsock,selectors.EVENT_READ,data=None)
        try:
            while self.shutdown==False:
                self.events = self.sel.select(timeout=None)
                for key,mask in self.events:
                    if(key.data is None):
                        self.accept_wrapper(key.fileobj)
                    else:
                        self.service_connection(key,mask)
        except:
            print("Exception: {}".format(traceback.print_exc()))
        finally:
            self.Shutdown()

    def Shutdown(self):
        self.shutdown=True
        self.sel.unregister(self.lsock)
        self.sel.close()
        self.lsock.close()

    def accept_wrapper(self,sock):
        conn, addr = sock.accept()  # Should be ready to read
        print("accepted connection from", addr)
        conn.setblocking(False)
        data = types.SimpleNamespace(addr=addr, inb=b"", outb=b"",stateObj=StateObject())
        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        self.sel.register(conn, events, data=data)

    def service_connection(self, key, mask):
        sock = key.fileobj
        data = key.data
        if mask & selectors.EVENT_READ:
            if(data.stateObj.expectedSzReceived == False):

                recv_data = sock.recv(INT_SIZE())
                if recv_data:
                    data.stateObj.expectedSzReceived = True
                    data.stateObj.expectedSz = convert_data_to_ints(recv_data)[0]
                    data.stateObj.szReceivedCount = INT_SIZE()

                else:
                    print("(1)closing connection to", data.addr)
                    self.sel.unregister(sock)
                    sock.close()
            else:
                remainingSz = data.stateObj.expectedSz-data.stateObj.szReceivedCount
                recv_data = sock.recv(remainingSz)
                if recv_data:
                    data.stateObj.buffer=recv_data
                    data.stateObj.szReceivedCount+=remainingSz
                    self.CompletedData(key)
                    data.stateObj.expectedSzReceived = False
                    data.stateObj.expectedSz=0
                    data.stateObj.buffer=b''
                    data.stateObj.szReceivedCount = 0
                else:
                    print("(2)closing connection to", data.addr)
                    self.sel.unregister(sock)
                    sock.close()


        if mask & selectors.EVENT_WRITE:
            #IF SOCKET IS READY FOR WRITING WRITE IT HERE -> PING?
            #if data.outb:
            #    print("echoing", repr(data.outb), "to", data.addr)
            #    sent = sock.send(data.outb)  # Should be ready to write
            #    data.outb = data.outb[sent:]
            pass

    def GetColorFromSpectrum(self,spectrum): 
        r=0
        g=0
        b=0
        spectrum=sorted(spectrum.items(), key=lambda x: x[1], reverse=True)
        #print(spectrum)
        #if(spectrum[0][1] > 30):#generic volume check

        highest=spectrum[0][0]
        #lowest=spectrum[len(spectrum)-1][0]


        if(highest == 0):#check for loudest index
            b=spectrum[0][1]
        elif(highest == 1):
            b=spectrum[0][1]
        elif(highest == 2):
            b=spectrum[0][1]
            g=spectrum[1][1]
        elif(highest == 3):
            r=spectrum[1][1]
            g=spectrum[0][1]
        elif(highest == 4):
            r=spectrum[0][1]
            g=spectrum[1][1]
        elif(highest == 5):
            r=spectrum[0][1]
            #g=spectrum[1][1]
        elif(highest == 6):
            r=spectrum[0][1]
        else:
            print("NO HIGHEST")
        
        
        r=(int)(r/100 * 255)
        g=(int)(g/100 * 255)
        b=(int)(b/100 * 255)
        return r,g,b
    ####
    def wheel(self,pos):
        # Input a value 0 to 255 to get a color value.
        # The colours are a transition r - g - b - back to r.
        if pos < 0 or pos > 255:
            r = g = b = 0
        elif pos < 85:
            r = int(pos * 3)
            g = int(255 - pos * 3)
            b = 0
        elif pos < 170:
            pos -= 85
            r = int(255 - pos * 3)
            g = 0
            b = int(pos * 3)
        else:
            pos -= 170
            r = 0
            g = int(pos * 3)
            b = int(255 - pos * 3)
        return (r, g, b)
 

    def UpdatePixelsInRange(self,offset,size,color):
        self.pixels[offset:offset+size] = np.full((size,3),color)
    

    def CompletedData(self,key):
        global ADDRESSABLE_COUNT,ADDRESSABLE_MODE
        stateObj = key.data.stateObj
        curPos = 0
        intSz = INT_SIZE()

        datatype=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
        curPos+=intSz

        if(datatype==0):
            channel=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
            curPos+=intSz

            value=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
            curPos+=intSz

            calc=(int)(value/100 * MAX_DUTY_CYCLE)
            
            pca.channels[channel].duty_cycle = calc
        elif(datatype==1):
        
            spectrumCount=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
            curPos+=intSz
            spectrum_data = {}

            if(self.last_spectrum_count != spectrumCount):
                self.last_spectrum_sizes={}
                zone_sz = (int)(ADDRESSABLE_COUNT / spectrumCount)
                zone_remainder = ADDRESSABLE_COUNT % spectrumCount
                zone_offset = 0
                for i in range(spectrumCount):
                    cur_zone_sz = zone_sz
                    if(zone_remainder > 0):
                        cur_zone_sz += 1
                        zone_remainder-=1
                    
                    self.last_spectrum_sizes[i] = (cur_zone_sz,zone_offset)
            
                    zone_offset += cur_zone_sz

            for i in range(0,spectrumCount):
                
                spectrum_data[i]=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
                cur_tuple = self.wheel((int)(spectrum_data[i]/100 * 255))

                curPos += intSz
        
                self.UpdatePixelsInRange(self.last_spectrum_sizes[i][1],self.last_spectrum_sizes[i][0],cur_tuple)
                #threading.Thread(target=self.UpdatePixelsInRange, args=(self.last_spectrum_sizes[i][1],self.last_spectrum_sizes[i][0],cur_tuple,)).start()


            
            #threading.Thread(target=self.pixels.show()).start()

            calc_r=(int)(self.mapping[spectrum_data[1]]/100 * MAX_DUTY_CYCLE)
            calc_g=(int)(self.mapping[spectrum_data[2]]/100 * MAX_DUTY_CYCLE)
            calc_b=(int)(self.mapping[spectrum_data[4]]/100 * MAX_DUTY_CYCLE)
            pca.channels[0].duty_cycle=calc_r
            pca.channels[1].duty_cycle=calc_g
            pca.channels[2].duty_cycle=calc_b


            #self.last_spectrum=spectrum_data
            self.last_spectrum_count = spectrumCount


                
        elif(datatype==3):
            red=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
            curPos+=intSz       
            green=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
            curPos+=intSz 
            blue=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
            curPos+=intSz

            calc_r=(int)(red/100 * MAX_DUTY_CYCLE)
            calc_g=(int)(green/100 * MAX_DUTY_CYCLE)
            calc_b=(int)(blue/100 * MAX_DUTY_CYCLE)
            #pca.channels[0].duty_cycle=calc_r
            #pca.channels[1].duty_cycle=calc_g
            #pca.channels[2].duty_cycle=calc_b

            if(calc_r==0):
                calc_r=100
            if(calc_g==0):
                calc_g=100
            if(calc_b==0):
                calc_b=100
            if(calc_r > 40000):
                calc_r=40000
            if(calc_g > 40000):
                calc_g=40000
            if(calc_b>40000):
                calc_b=40000
            pca.channels[13].duty_cycle=calc_r
            pca.channels[14].duty_cycle=calc_g
            pca.channels[15].duty_cycle=calc_b

            #========================================
            #calc_r=(int)(red/100 * 255)
            #calc_g=(int)(green/100 * 255)
            #calc_b=(int)(blue/100 * 255)
            
            #for i in range(ADDRESSABLE_COUNT-1, 0, -1 ):
            #    self.pixels[i] = self.pixels[i-1]

            #self.pixels[0:8] = np.full((8,3),(calc_r,calc_g,calc_b),dtype=int)
            
            #self.pixels.show()


        elif(datatype==4):
        
            spectrumCount=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
            curPos+=intSz
            spectrum_data = {}


            for i in range(0,spectrumCount):
                spectrum_data[i]=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
                curPos += intSz
            
            for i in range(ADDRESSABLE_COUNT-1,7,-1):
                self.pixels[i] = self.pixels[i-8]


            for i in range(0,8,1):
                self.pixels[i] = ((int)(spectrum_data[4]/100 * 255),(int)(spectrum_data[2]/100 * 255),(int)(spectrum_data[1]/100 * 255))

            self.pixels.show()
            calc_r=(int)(self.mapping[spectrum_data[1]]/100 * MAX_DUTY_CYCLE)
            calc_g=(int)(self.mapping[spectrum_data[2]]/100 * MAX_DUTY_CYCLE)
            calc_b=(int)(self.mapping[spectrum_data[4]]/100 * MAX_DUTY_CYCLE)
            pca.channels[0].duty_cycle=calc_r
            pca.channels[1].duty_cycle=calc_g
            pca.channels[2].duty_cycle=calc_b

        elif(datatype==200):
            ADDRESSABLE_MODE=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
            curPos += intSz
                
        elif(datatype==300):
            ADDRESSABLE_COUNT=(int)(convert_data_to_ints(stateObj.buffer[curPos:curPos+intSz])[0])
            curPos += intSz
