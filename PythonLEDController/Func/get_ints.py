import ctypes
from struct import *


def convert_data_to_ints(data, big_endian=False):
    int_count = len(data) // INT_SIZE()  # Assuming uint is 4 bytes long !!!
    fmt = ">" if big_endian else "<"
    fmt += "I" * int_count
    return unpack(fmt, data[:int_count * INT_SIZE()])

def INT_SIZE():
    return ctypes.sizeof(ctypes.c_int)