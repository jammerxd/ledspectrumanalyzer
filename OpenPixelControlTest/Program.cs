﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenPixelControlTest
{
    class Program
    {
        static AsyncTCP.Client.AsynchronousClient client;
        static void Main(string[] args)
        {
            client = new AsyncTCP.Client.AsynchronousClient();
            client.Connected += Client_Connected;
            client.Disconnected += Client_Disconnected;
            client.Error += Client_Error;
            client.NewData += Client_NewData;

            client.Connect("10.0.0.93",7890,System.Net.Sockets.ProtocolType.Udp);
            Console.ReadLine();
            
        }

        private static void Client_NewData(AsyncTCP.DataClass e)
        {
            
        }

        private static void Client_Error(AsyncTCP.ClientEventDetails e)
        {
            Console.WriteLine(String.Format("Error {0}", e.Exception?.Message));
        }

        private static void Client_Disconnected(AsyncTCP.ClientEventDetails e)
        {
            Console.WriteLine("Disconnected");
        }

        private static void Client_Connected(AsyncTCP.ClientEventDetails e)
        {
            int pixelCount = 300;
            Console.WriteLine("Connected");
            
            try
            {
                client.SendBytes(BuildColor(255,255,255,300).ToArray(), false);
                System.Threading.Thread.Sleep(5000);
                client.SendBytes(BuildColor(255, 0, 0, 300).ToArray(), false);
                System.Threading.Thread.Sleep(5000);
                client.SendBytes(BuildColor(0, 255, 0, 300).ToArray(), false);
                System.Threading.Thread.Sleep(5000);
                client.SendBytes(BuildColor(0, 0, 255, 300).ToArray(), false);
                System.Threading.Thread.Sleep(5000);
                client.SendBytes(BuildColor(255, 255, 255, 300).ToArray(), false);

                List<Color> pixels = new List<Color>();
                for(int i = 0; i < pixelCount; i++)
                {
                    pixels.Add(Color.FromArgb(0, 0, 0));
                }

                int counter = 0;
                while(true)
                {
                    for(int i = 0; i < pixelCount-1; i++)
                    {
                        pixels[i] = pixels[i + 1];
                    }
                    pixels[pixelCount-1] = Color.FromArgb(0, 0, 255);
                    client.SendBytes(BuildPixels(pixels).ToArray(), false);
                    if(counter >= pixelCount+100)
                    {
                        counter = 0;
                        Console.WriteLine("RESET");
                        for(int i = 0; i < pixelCount; i++)
                        {
                            pixels[i] = Color.FromArgb(0, 0, 0);
                        }
                        client.SendBytes(BuildPixels(pixels).ToArray(), false);
                        System.Threading.Thread.Sleep(1000);
                        
                    }
                    counter++;
                    System.Threading.Thread.Sleep(16);

                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            client.Disconnect();
            Console.WriteLine("DONE");
        }
        private static IEnumerable<byte> GetBytesFromColor(int r, int g, int b,int count)
        {

            List<byte> bytes = new List<byte>();

            List<byte> single = new List<byte>();
            single.AddRange(new List<byte>() { (byte)r, (byte)g, (byte)b });

            for(int i = 0; i<count;i++)
            {
                bytes.AddRange(single);
            }

            return bytes;
        }

        private static IEnumerable<byte> BuildColor(int r, int g, int b, int count)
        {
            var lenHiByte = (count * 3) / 256;
            var lenLoByte = (count * 3) % 256;
            byte[] header = { (byte)1, (byte)0, (byte)lenHiByte, (byte)lenLoByte };
            List<byte> bytesToSend = new List<byte>();
            bytesToSend.AddRange(header);
            bytesToSend.AddRange(GetBytesFromColor(r, g, b, count));
            return bytesToSend;

        }
        private static IEnumerable<byte> BuildPixels(IEnumerable<Color> pixels)
        {
            List<byte> bytesToSend = new List<byte>();
            var lenHiByte = (pixels.Count() * 3) / 256;
            var lenLoByte = (pixels.Count() * 3) % 256;
            byte[] header = { (byte)1, (byte)0, (byte)lenHiByte, (byte)lenLoByte };
            bytesToSend.AddRange(header);
            foreach(var pixel in pixels)
            {
                bytesToSend.AddRange(new List<byte>() { (byte)pixel.R, (byte)pixel.G, (byte)pixel.B });
            }
            return bytesToSend;

        }
        
    }
}
